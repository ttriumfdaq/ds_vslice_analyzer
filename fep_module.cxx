/******************************************************************
 * Mock Front-End Processing *
 * 
 * A. Capra
 * October 2022
 *
 ******************************************************************/

#include "manalyzer.h"
#include "midasio.h"
#include "mjson.h"

#include "dsvsflow.h"
#include "dsdata.h"

#include <iostream>
#include <fstream>
#include <cmath>
#include <numeric>
#include <functional>

#include "json.hpp"
using json = nlohmann::json;


class FEProcFlags
{
public:
   bool fVerbose;
   bool fDebug;
   std::string fConfigName;
   FEProcFlags():fVerbose(false),fDebug(false)
   {
      std::string basename=BASEDIRECTORY;
      fConfigName=basename+"/config/master.json";
   }
};
  

class FEProcModule: public TARunObject
{
public:
   FEProcFlags* fFlags;

private:
   int fCounter; // counts the number of processed events
   int fError;   // counts the number of encountered errors
   
   // // Length of filtered waveform baseline
   // int nbase_samples; // Pedestal;
   
   // flip wf ?
   bool fInvert;
   int fOffset;

   //   std::vector<float> fBaselines;

   // AR filter
   bool fARF=false;
   uint16_t ftau;
   // // convenience variables
   // int32_t _a;
   // int32_t _tau;

   // hit finder
   int32_t fThreshold;
   uint16_t fWindow;
   int32_t fMinQA;
   // convenience variables
   uint16_t fHalfWindow;

   // waveforms
   std::vector<int32_t>* fbaseline_subtracted;
   std::vector<int32_t>* ffiltered_wf;

   // statistics variables
   unsigned fTotSamples;
   unsigned fTotPulses;
   unsigned fNHits;

public:
   FEProcModule(TARunInfo* runinfo, FEProcFlags* f): TARunObject(runinfo),
                                                     fFlags(f), fCounter(0), fError(0)
   {
      if(fFlags->fVerbose) std::cout<<"FEProcModule ctor"<<std::endl;
#ifdef HAVE_MANALYZER_PROFILER
      fModuleName="FEProc";
#endif

      std::ifstream fin(fFlags->fConfigName.c_str());
      json settings;
      fin>>settings;
      if(fFlags->fVerbose)
         std::cout<<"FEProcModule Json parsing success!"<<std::endl;
      fin.close();

      fInvert = settings["Baseline"]["Invert"].get<bool>();
      std::cout<<"FEProcModule ctor Flip Signal: "<<fInvert<<std::endl;

      std::string filter_name = settings["Filter"]["Name"].get<std::string>();
      if( filter_name == "AR" )
         {
            fARF = true;
            ftau = settings["Filter"]["Par0"].get<uint16_t>();
            std::cout<<"FEProcModule ctor "<<filter_name<<" filter tau: "<<ftau<<std::endl;
           
         }

      fWindow = settings["Pulse"]["MAgate"].get<uint16_t>();
      fHalfWindow = fWindow/2;
      std::cout<<"FEProcModule MA gate: "<<fWindow<<" ("<<fHalfWindow<<")"<<std::endl;
      fThreshold = settings["Pulse"]["Amplitude"].get<int32_t>();
      std::cout<<"FEProcModule Threshold: "<<fThreshold<<std::endl;
      fMinQA = settings["Pulse"]["MinCharge"].get<int32_t>();
      std::cout<<"FEProcModule Charge/Prominence Cut: "<<fMinQA<<std::endl;

      fTotSamples = fTotPulses = fNHits = 0;
   }


   void BeginRun(TARunInfo* runinfo)
   {
      if(runinfo->fFileName.empty() )
         std::cout<<"FEProcModule::BeginRun() run: "<<runinfo->fRunNo<<" online"<<std::endl;
      else
         std::cout<<"FEProcModule::BeginRun() run: "<<runinfo->fRunNo<<" midasfile: "<<runinfo->fFileName.c_str()<<std::endl;

      fbaseline_subtracted = new std::vector<int32_t>;
      ffiltered_wf = new std::vector<int32_t>;

      // runinfo->fOdb->RFA("/VX2740 defaults/DC offset (pct)",&fBaselines);
      // float dynamic_range = pow(2,16);
      // for(auto& b: fBaselines)
      //    {
      //       b *= dynamic_range/100;
      //       std::cout<<b<<", ";
      //    }
      // std::cout<<"\n";
   }

   void EndRun(TARunInfo* runinfo)
   {
      delete fbaseline_subtracted;
      delete ffiltered_wf;
      std::cout<<"FEProcModule::EndRun() run: "<<runinfo->fRunNo<<" events: "<<fCounter<<" errors: "<<fError
               <<"\tTotal number of Samples: "<<fTotSamples<<"  Total Number of Pulses: "<<fTotPulses
               <<" Total Number of Hits: "<<fNHits<<std::endl;
   }

  
   TAFlowEvent* AnalyzeFlowEvent(TARunInfo* /*runinfo*/, TAFlags* flags, TAFlowEvent* flow)
   {
      SliceEventFlow* sef = flow->Find<SliceEventFlow>();
      if( !sef )
         {
#ifdef HAVE_MANALYZER_PROFILER
            *flags|=TAFlag_SKIP_PROFILE;
#endif
            ++fError;
            return flow;
         }

      std::cout<<"FEProcModule::AnalyzeFlowEvent # "<<fCounter
               <<"\t # of slices: "<<sef->fNslices<<std::endl;

      std::vector<SlicePulse*>* pulses = sef->fslice_pulse;
      if( !pulses )
         {
#ifdef HAVE_MANALYZER_PROFILER
            *flags|=TAFlag_SKIP_PROFILE;
#endif
            return flow;
         }
      
      if( pulses->size() == 0 ) return flow;

      sef->fhits = new std::vector<DSVSHit*>;
     
      for(auto it=pulses->begin(); it!=pulses->end(); ++it)
         {
            SlicePulse* aPulse = *it;
            // baseline subtraction + AR filter + MA subtraction
            filter( aPulse->GetWaveform(), aPulse->baseline, fbaseline_subtracted, ffiltered_wf);

            // std::cout<<"FEProcModule:: "<<aPulse->GetIndex()<<" --> "
            //          <<*std::min_element(fbaseline_subtracted->begin(),fbaseline_subtracted->end())<<"\t"
            //          <<*std::max_element(fbaseline_subtracted->begin(),fbaseline_subtracted->end())<<"\t"
            //          <<*std::min_element(ffiltered_wf->begin(),ffiltered_wf->end())<<"\t"
            //          <<*std::max_element(ffiltered_wf->begin(),ffiltered_wf->end())<<std::endl;
            
            // peak finder
            peak_finder(fbaseline_subtracted, ffiltered_wf, aPulse->GetIndex(), aPulse->wfts, sef->fhits);

            // accumulate statistics
            ++fTotPulses;
            fTotSamples += aPulse->GetWaveform()->size();
                
            fbaseline_subtracted->clear();
            ffiltered_wf->clear();
         }// pulse loop

      //fNHits += sef->fhits->size();
      std::cout<<"FEProcModule::AnalyzeFlowEvent # "<<fCounter
               <<"\t # hits: "<<sef->fhits->size()<<std::endl;
     
      ++fCounter;
      return flow;
   }

   void filter(const std::vector<uint16_t>* in, const uint16_t& baseline, std::vector<int32_t>* wf, std::vector<int32_t>* out)
   {
      // initialize
      const size_t nsamples = in->size();
      const uint16_t* adc_samples = in->data();
      
      // prepare output
      // baseline subtracted
      wf->resize(nsamples);
      int32_t* wfa = wf->data();
      // AR filtered and MA subtracted
      out->resize(nsamples);
      int32_t* hit = out->data(); // AR-MA
      
      // prepare AR kernel
      int32_t AR[nsamples];
      if( fInvert ) AR[0] = static_cast<int32_t>(baseline - adc_samples[nsamples-1]);
      else AR[0] = static_cast<int32_t>(adc_samples[nsamples-1]-baseline);

      // helper variables
      size_t i, j=1, n; // running index
      int32_t y; // baseline subtracted waveform
      int32_t flt[nsamples]; // AR filtered
      int32_t asum = 0; // partial sum
      size_t center_window = nsamples - fHalfWindow;
      
      // Baseline Subtraction and Auto-Recursive kernel generation
      for( i=1; i<nsamples; ++i)
         {
            if( fInvert ) y = static_cast<int32_t>(baseline-adc_samples[nsamples-1-i]);
            else y = static_cast<int32_t>(adc_samples[nsamples-1-i]-baseline);
            wfa[nsamples-1-i] = y;
            AR[i] = y + AR[i - 1] - AR[i - 1]/ftau;
         }
      if( fInvert ) wfa[nsamples-1] = static_cast<int32_t>(baseline - adc_samples[nsamples-1]);
      else wfa[nsamples-1] = static_cast<int32_t>(adc_samples[nsamples-1]-baseline);
      
      // -------------------------------------------------------
      // create output:
      // Auto Recursive filtering and Moving Average Subtraction
      // -------------------------------------------------------
      // MA and subtraction first point and
      // filter [0,0.5*MA window) in half window steps
      for( i=0; i<fHalfWindow; ++i )
         {
            flt[i] = AR[ nsamples - 1 - i ] / ftau; // AR filter
            asum += 2*flt[i]; 
         }
      hit[0] = flt[0] - asum/fWindow;
      // -------------------------------------------------------
      // MA and subtraction in first half window and
      // filter [0.5*MA window, MA window) in half window steps
      for( i=1; i<=fHalfWindow; ++i )
         {
            n = center_window - i;  // index in AR vector
            flt[ i + fHalfWindow - 1 ] = AR[ n ] / ftau; // AR filter
            asum += flt[ i + fHalfWindow - 1 ] - flt[ fHalfWindow - i ];
            hit[i] = flt[i] - asum/fWindow; // MA subtraction
         }
      // -------------------------------------------------------
      // MA and subtraction from half window to number of samples minus half window
      // and filter [MA window, N samples) in N samples - 2*MA windows steps
      for( i=fHalfWindow+1; i<=center_window; ++i )
         {
            n = center_window - i; // index in AR vector
            flt[ i + fHalfWindow - 1 ] = AR[ n ] / ftau; // AR filter
            asum += flt[ i + fHalfWindow - 1 ] - flt[ i - fHalfWindow - 1 ];
            hit[i] = flt[i] - asum/fWindow;  // MA subtraction
         }
      // -------------------------------------------------------
      // MA and subtraction from number of samples minus half window to number of samples
      // and filter is already done
      for( i=center_window+1; i<nsamples; ++i )
         {
            asum += flt[ nsamples - j++ ] - flt[ i - fHalfWindow - 1 ];
            hit[i] = flt[i] - asum/fWindow;  // MA subtraction
         }
      // -------------------------------------------------------
   }

   void peak_finder(const std::vector<int32_t>* wf, const std::vector<int32_t>* in,
                    const uint16_t& ch_index, const double& ts, std::vector<DSVSHit*>* out)
   {
      auto it = in->begin();
      int32_t thr = fThreshold;
      if( fFlags->fDebug ) std::cout<<ch_index<<" --> "<<*std::min_element(in->begin(),in->end())<<std::endl;
      while(true)
         {
            // find where waveform is above thr
            it = std::find_if( it, in->end(),
                               [thr](int32_t const& r)
                               { return (r>thr); } );
            // if it's never above thr, quit
            if( it==in->end() ) break;
	
            uint32_t iStart = std::distance(in->begin(),it);
            if( fFlags->fDebug ) std::cout<<"\t["<<iStart;

            // find where waveform goes back below thr
            it = std::find_if( it, in->end(),
                               [thr](int32_t const& r)
                               { return (r<thr); } );
            uint32_t iEnd = std::distance(in->begin(),it);
            if( fFlags->fDebug ) std::cout<<" "<<iEnd<<"]";

            // find peak pulse
            std::vector<int32_t>::const_iterator max_pos = std::max_element(in->begin()+iStart,
                                                                            in->begin()+iEnd);
            int32_t A = *max_pos;
            if( fFlags->fDebug ) std::cout<<" A: "<<A;
	
            // if the pulse is too 'small'
            int32_t Q = std::accumulate(in->begin()+iStart,in->begin()+iEnd,int32_t(0));
            if( fFlags->fDebug ) std::cout<<" Q: "<<Q<<"\tcut: "<<Q/A;
            if( Q/A < fMinQA )
               {
                  ++it;
                  if( fFlags->fDebug ) std::cout<<" too small"<<std::endl;
                  continue;
               }

            //// peak-width
            // uint32_t duration = iEnd-iStart;

            // peak time (sample)
            uint16_t sample = std::distance(in->begin(),max_pos);
            if( fFlags->fDebug ) std::cout<<" t: "<<sample;

            // amplitude of filtered wf
            uint16_t prom = wf->at(sample);
            if( fFlags->fDebug ) std::cout<<" prom: "<<prom<<std::endl;

            DSVSHit* aHit = new DSVSHit(ch_index, ts);
            aHit->time = sample;
            aHit->height = prom;
            if( fFlags->fVerbose ) aHit->Print();
            out->push_back( aHit );
            ++fNHits;
            ++it;
         }
   }
};


class FEProcModuleFactory: public TAFactory
{
public:
   FEProcFlags fFlags;
   
public:
   void Usage()
   {
      //      printf("Modules options flags:\n");
      printf("\n");
      printf("\t--config, --conf, -c </path/to/json_file>\n\t\tspecify path json configuration file\n");
      printf("\t--verbose, -v\n\t\tprint status information\n");
      printf("\t--debug, -d\n\t\tprint detailed information\n");
   }

   void Init(const std::vector<std::string> &args)
   {
      printf("FEProcModuleFactory::Init!\n");
      
      for (unsigned i=0; i<args.size(); i++)
         {
            if( args[i] == "--verbose" ||
                args[i] == "-v" )
               fFlags.fVerbose = true;
            if( args[i] == "--debug" ||
                args[i] == "-d" )
               fFlags.fDebug = true;
            if( args[i] == "--config" ||
                args[i] == "--conf" ||
                args[i] == "-c" )
               fFlags.fConfigName=args[i+1];
         }
   }
   
   void Finish()
   {
      printf("FEProcModuleFactory::Finish!\n");
   }
   
   TARunObject* NewRunObject(TARunInfo* runinfo)
   {
      printf("FEProcModuleFactory::NewRunObject, run %d, file %s\n", runinfo->fRunNo, runinfo->fFileName.c_str());
      return new FEProcModule(runinfo, &fFlags);
   }

};

static TARegister tar(new FEProcModuleFactory);

/* emacs
 * Local Variables:
 * tab-width: 8
 * c-basic-offset: 3
 * indent-tabs-mode: nil
 * End:
 */
