# Darkside TRIUMF Vertical Slice Analyzer

## Prerequisites

- [ROOT](https://root.cern.ch), tested with v.6.24/02
    - requires `xml` feature

- [MIDAS](https://midas.triumf.ca), not necessary for offline processing
    - requires `cmake` version >= 3.12

- [CMake](https://cmake.org), this is the primary configuration tool

Detailed steps for building the prerequisites on dsvslice can be found [here](setup_analyzer.md)


## Building procedure

Assuming that the source code is cloned in a directory called `ds_vslice_analyzer`
```
cd ds_vslice_analyzer
mkdir build && cd build
cmake ..
```

This operation also clones all the necessary submodules.

```
cmake --build . --target install
```

The executable is copied to `ds_vslice_analyzer/bin`


## Run

```
cd bin
./dsvsana.exe -O../demoR1031.root /home/dsdaq/online/data/run001031_tsp001_subrun0000.mid  -- --tsphisto --verbose
```

Now the rootfile `demoR1031.root` can be processed with standard tools.
