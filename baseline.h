/******************************************************************
 *  Baseline algorithms *
 * 
 * A. Capra
 * March 2022
 *
 ******************************************************************/

#include <vector>
#include <numeric>
#include <algorithm>
#include <cstdint>
#include <cmath>
//#include <iostream>

// ****************************************************************

// template function to calculate mean of vector
template <typename T>
T _mean(const std::vector<T>* v, size_t length = 100)
{
  T m = std::accumulate(v->begin(), v->end(), T(0));
  return m / (T) length;
}

// template function to calculate root-mean-square of vector
// requires knowledge of the mean
// length used here should the same as the one use to calculate the mean
template <typename T, typename U>
float _rms(const std::vector<T>* v, U& mean, size_t length = 100)
{
  float rms=(float)std::inner_product(v->begin(), v->end(), v->begin(), float(0));
  //  std::cout<<rms<<std::endl;
  return sqrt( (rms / (float) length) - float(mean * mean) );
}

// combine the two functions above
template <typename T, typename U>
void get_mean_and_rms(const std::vector<T>* v, T& mean, U& rms, size_t length = 100)
{
  mean = _mean<T>( v, length);
  rms = _rms<T,T>( v, mean, length );
}

// template function to calculate the median of a vector
template <typename T>
T median(const std::vector<T>* v)
{
  std::vector<T> w(*v); // since we are modifying the vector later, we make a local copy
  size_t n = w.size() / 2; // position of the median
  // !!! wow !!!
  // Rearranges the elements in the range such that the second argument
  // is the element that would be in that position in a sorted sequence.
  // https://www.cplusplus.com/reference/algorithm/nth_element/
  std::nth_element(w.begin(), w.begin()+n, w.end());
  return w[n]; // the median
}

// template function to calculate the mode of a vector
template <typename T>
T most_probable_value(const std::vector<T>* v)
{
  // create a counter, i.e., a map from the vector element to its (absolute) frequency
  std::map<T, unsigned> counters;
  // count how many times an item appears
  for(auto& i: *v) ++counters[i];
  
  // get the map value that is most frequent
  auto pr = std::max_element( counters.begin(), counters.end(),
			      [] (const std::pair<T,unsigned>& p1, const std::pair<T,unsigned>& p2) { // lambda function
				return p1.second < p2.second;}  // strict ordering
			      );
  return pr->first;
}
  
// ****************************************************************

// template function to get baseline by slicing the wf where desired
// compute the baseline starting at sample "start" for "gate" samples
template <typename T>
T get_baseline( const std::vector<T>* v, int mode = 3, size_t gate = 100, size_t start = 0 )
{
   if( mode == 1 ) // median 
    {
      return median<T>( v );
    }
   else if ( mode == 2 ) // mode
    {
      return most_probable_value<T>( v );
    }
   else if( mode == 3 ) // mean over 'gate' samples
    {
      const std::vector<T> w(v->begin()+start,v->begin()+start+gate);
      return _mean<T>( &w, gate );
    }
  else
    return 0;
}

// template function to get baseline and its rms by slicing the wf where desired
// compute the baseline starting at sample "start" for "gate" samples
template <typename T, typename U>
void get_baseline_and_rms( const std::vector<T>* v, T& mean, U& rms, size_t gate = 100, size_t start = 0 )
{
  const std::vector<T> w(v->begin()+start,v->begin()+start+gate);
  get_mean_and_rms<T,U>(&w, mean, rms, gate);
}
// ****************************************************************


template <typename T, typename U>
std::vector<T> get_subtracted_inverted(const std::vector<U>* v, uint16_t& baseline, bool inv=false)
{
  std::vector<T> w(v->size());
  T bline = static_cast<T>(baseline);
  if( inv )
    std::transform(v->begin(),v->end(),w.begin(),
		   [bline](U y){return -1*(static_cast<T>(y)-bline);});
  else
    std::transform(v->begin(),v->end(),w.begin(),
		   [bline](U y){return static_cast<T>(y)-bline;});
  return w;
}

// fixed type extraction of the waveform around zero
// and inversion for simpler prominence extraction
// mode 0: no baseline subtraction, only inversion
// mode 1: baseline subtraction using median
// mode 2: baseline subtraction using most probable value
// mode 3: baseline subtraction standard
template <typename T, typename U>
std::vector<T> get_baseline_subtracted_inverted(const std::vector<U>* v, int16_t& baseline, int mode=3, size_t gate = 100, bool inv=false)
{
  int16_t b = (int16_t) get_baseline<T>(v, mode, gate);
  baseline = b;
  return get_subtracted_inverted<T,U>( v, b, inv );
}


/* emacs
 * Local Variables:
 * tab-width: 8
 * c-basic-offset: 3
 * indent-tabs-mode: nil
 * End:
 */
