// *******************************************************
// Analyzer module for TRIUMF DarkSide Vertical Slice
// This module reads time slice data from a MIDAS bank,
// decode them and makes them available in the data flow
//
// A. Capra - TRIUMF
// July 2021
//
// *******************************************************

#include <iostream>
#include <string>
#include <vector>
#include <set>
#include <map>

#include "data_structures.h" // from ds_vslice
#include "manalyzer.h"
#include "midasio.h"

#include "dsvsflow.h" // data to pass to other modules


class SliceDecoderFlags
{
   // command line arguments to modify the behaviour of this module
public:
   bool fVerb=false;
};


class SliceDecoderModule: public TARunObject
{
private:
   SliceDecoderFlags* fFlags;
   Slice* fSlice; 
   int fCounter; // counts the number of processed events
   int fError;   // counts the number of encoutered errors
   int fSliceCounter; // counts the number of decoded slices

public:
   SliceDecoderModule(TARunInfo* runinfo, SliceDecoderFlags* flags):
      TARunObject(runinfo), fFlags(flags), fSlice(0),
      fCounter(0), fError(0), fSliceCounter(0)
   {
      if(fFlags->fVerb) std::cout<<"SliceDecoderModule ctor"<<std::endl;
#ifdef HAVE_MANALYZER_PROFILER
      fModuleName="SliceDecoder";
#endif
   }

   ~SliceDecoderModule()
   {
      if(fFlags->fVerb) std::cout<<"SliceDecoderModule dtor"<<std::endl;
   }

   void BeginRun(TARunInfo* runinfo)
   {
      if(runinfo->fFileName.empty() )
         std::cout<<"SliceDecoderModule::BeginRun() run: "<<runinfo->fRunNo<<" online"<<std::endl;
      else
         std::cout<<"SliceDecoderModule::BeginRun() run: "<<runinfo->fRunNo<<" midasfile: "<<runinfo->fFileName.c_str()<<std::endl;

      fSlice = new Slice();
      std::cout<<"SliceDecoderModule::BeginRun() new Slice!"<<std::endl;
   }

   void EndRun(TARunInfo* runinfo)
   {
      std::cout<<"SliceDecoderModule::EndRun() run: "<<runinfo->fRunNo<<" events: "<<fCounter<<" errors: "<<fError<<" slices: "<<fSliceCounter<<std::endl;
      delete fSlice;
      std::cout<<"SliceDecoderModule::EndRun() Slice deleted"<<std::endl;
   }

   
   TAFlowEvent* Analyze(TARunInfo* /*runinfo*/, TMEvent* midasevent,
                        TAFlags* /*flags*/, TAFlowEvent* flow)
   {
      if( midasevent->error )
         {
            ++fError;
            return flow;
         }
      
      if( fFlags->fVerb )
         std::cout<<"SliceDecoderModule::Analyze ID: "<<midasevent->event_id
                  <<" S/N: "<<midasevent->serial_number
                  <<" TS: "<<midasevent->time_stamp
                  <<" size: "<<midasevent->data_size<<std::endl;

      midasevent->FindAllBanks();

      // store the data from the current event
      // for further processing
      unsigned int nSlices=0;
      std::vector<SlicePulse*> spulse;
      // determine the number of ADCs and FEPs
      std::set<int> adc_set;
      std::set<int> fep_set;
      // each "event" is a slice coming from a particular TSP node
      // therefore the tsp id should be fixed for the FlowEvent
      int tspid=-1; // TSP S/N
      uint32_t slice_index = 0; // Time-Slice-Marker index
      double slice_start = -1.; // Time-Slice-Marker Time-Stamp
      for(size_t i=0; i<midasevent->banks.size(); ++i)
         {
            const TMBank* b = &midasevent->banks[i];
            // BANK name format:
            // TSP: Tijk where 'ijk' is a three digits number
            // FEP: Fijk where 'ijk' is a three digits number
            if( !std::isdigit( b->name[1] ) ) continue; // if 'i' is not a digit, it's the wrong bank
            if( b->name[0]=='T' ) // TSP bank
               {
                  // TSP data bank always begin with a 'T'
                  //if( fFlags->fVerb )
                  std::cout<<"SliceDecoderModule::Analyze TSP Data Bank Found "<<b->name<<std::endl;
                  tspid = std::stoi(b->name.substr(1,3));
               }
            else if( b->name[0]=='F' ) // FEP bank
               {
                  // FEP data bank always begin with an 'F'
                  //if( fFlags->fVerb )
                  std::cout<<"FEP Data Bank Found "<<b->name<<std::endl;
               }
            else
               continue;

            const char* bkptr = midasevent->GetBankData(b);
            uint32_t data_size = b->data_size;
                        
            // decode binary data into Slice object
            // print error if any
            if( !fSlice->decode( (uint64_t*) bkptr, data_size ) ) // reset is called inside
               {
                  std::cerr<<"SliceDecoderModule::Analyze "<<fSlice->error_message<<std::endl;
                  ++fError;
                  return flow;
               }
            ++fSliceCounter;
            
            // get Time-Slice-Marker information
            slice_index = fSlice->slice_idx; 
            slice_start = fSlice->slice_start_time_secs;

            // set of interesting channels ordered by timestamp
            std::vector<std::shared_ptr<HitWithChannelId>> hit_data = fSlice->get_hits_sorted_by_time();
            int iwf=0; // pulse index

            // loop over channels: get their id and waveforms (pulses)
            for(auto it=hit_data.begin(); it!=hit_data.end(); ++it)
               {
                  ChannelId aChan = (*it)->chan;
                  if( is_waveform((*it)->hit) ) // verify that is the correct obj
                     {
                        // get wf
                        std::shared_ptr<Waveform> wf = std::dynamic_pointer_cast<Waveform>((*it)->hit);
                        if( fFlags->fVerb && 0 )
                           {
                              std::cout<<"SliceDecoderModule::Analyze HitWithChannelId is waveform and has "
                                       <<wf.get()->samples.size()<<" samples"<<std::endl;
                           }
                        
                        // std::vector<std::shared_ptr<Waveform>> pulses = fSlice->channel_waveforms[aChan];
                        adc_set.insert(aChan.board_id);
                        fep_set.insert(aChan.frontend_id);
                        
                        // fill my object to put the waveform in the flow
                        spulse.push_back( new SlicePulse(tspid,
                                                         aChan.frontend_id,
                                                         +aChan.board_id,
                                                         +aChan.channel_id,
                                                         iwf,
                                                         &wf.get()->samples,
                                                         slice_index,
                                                         slice_start,
                                                         wf.get()->time_since_run_start_secs
                                                         ) );
                        ++iwf;
                     }// is wf
                  else
                     {
                        std::cout<<"SliceDecoderModule::Analyze not a wf"<<std::endl;
                     }
               } // loop over hits
            ++nSlices;

            //if( fFlags->fVerb )
            std::cout<<"SliceDecoderModule::Analyze TSP: "<<tspid<<"\t MTS: "<<midasevent->time_stamp<<"\tSlice Index: "<<slice_index<<"\tSlice Start: "<<slice_start<<" N hits: "<<hit_data.size()<<" N pulses: "<<iwf<<std::endl;
            
         }// loop over MIDAS banks

      // put *some* data in the flow
      SliceEventFlow* flowevent=new SliceEventFlow(flow,
                                                   adc_set.size(),fep_set.size(),nSlices,
                                                   midasevent->time_stamp, midasevent->serial_number);
      flowevent->fTSPindex = tspid;         // set TPS serial number
      flowevent->fSliceIdx = slice_index;   // set slice index
      flowevent->fSliceStart = slice_start; // set slice ts
      flowevent->fslice_pulse = new std::vector<SlicePulse*>(spulse); // store custom hit object
      flow = flowevent;
      
      ++fCounter;
      return flow;
   }
};


class SliceDecoderFactory: public TAFactory
{
private:
   SliceDecoderFlags fFlags;

public:
   void Init(const std::vector<std::string> &args)
   {
      std::cout<<"SliceDecoderFactory::Init()"<<std::endl;
      for(size_t i=0; i<args.size(); ++i)
         {
            if( args[i] == "--verbose"  ||
                args[i] == "-v" )
               fFlags.fVerb=true;
         }
   }
   void Usage()
   {
     std::cout<<"SliceDecoder flags:"<<std::endl;
     std::cout<<"--verbose show what's happening"<<std::endl;
   }
   void Finish()
   {
      std::cout<<"SliceDecoderFactory::Finish()"<<std::endl;
   }

   TARunObject* NewRunObject(TARunInfo* runinfo)
   {
      std::cout<<"SliceDecoderFactory::NewRunObject()"<<std::endl;
      return new SliceDecoderModule(runinfo,&fFlags);
   }
};

static TARegister tar(new SliceDecoderFactory);

/* emacs
 * Local Variables:
 * tab-width: 8
 * c-basic-offset: 3
 * indent-tabs-mode: nil
 * End:
 */
