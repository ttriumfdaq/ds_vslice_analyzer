void FEPfilterI(const std::vector<uint16_t>* in,
		uint16_t& baseline, uint16_t& ftau, uint32_t& fWindow,
		std::vector<int32_t>* wf, std::vector<int32_t>* out)
{
  // initialize
  size_t nsamples=in->size();
  const uint16_t* adc_samples = in->data();
  std::cout<<"tau: "<<ftau<<std::endl;
  uint32_t fHalfWindow = static_cast<uint32_t>(fWindow/2);
      
  // prepare output
  // baseline subtracted
  wf->resize(nsamples);
  int32_t* wfa = wf->data();
  // AR filtered and MA subtracted
  out->resize(nsamples);
  int32_t* hit = out->data(); // AR-MA
      
  // prepare AR kernel
  int32_t AR[nsamples];
  //AR[0] = static_cast<double>(adc_samples[nsamples-1]-baseline);
  AR[0] = static_cast<int32_t>(baseline-adc_samples[nsamples-1]);

  // helper variables
  size_t i, j=1, n; // running index
  int32_t y; // baseline subtracted waveform
  int32_t flt[nsamples]; // AR filtered
  int32_t asum = 0; // partial sum
  size_t center_window = nsamples - fHalfWindow;
  size_t maxn = nsamples - 1;
      
  // Baseline Subtraction and Auto-Recursive kernel generation
  for( i=1; i<nsamples; ++i)
    {
      //y = static_cast<double>(adc_samples[nsamples-1-i]-baseline);
      y = static_cast<int32_t>(baseline - adc_samples[nsamples-1-i]);
      wfa[nsamples-1-i] = y;
      AR[i] = y + AR[i - 1] - AR[i - 1]/ftau;
      //      std::cout<<"("<<i<<","<<y<<","<<AR[i]<<") ";
    }
  //std::cout<<"\n";
  wfa[nsamples-1] = static_cast<int32_t>(baseline - adc_samples[nsamples-1]);
  //wfa[nsamples-1] = static_cast<int32_t>(adc_samples[nsamples-1]-baseline);

  // -------------------------------------------------------
  // create output:
  // Auto Recursive filtering and Moving Average Subtraction
  // -------------------------------------------------------
  // MA and subtraction first point and
  // filter [0,0.5*MA window) in half window steps
  for( i=0; i<fHalfWindow; ++i )
    {
      flt[i] = AR[ maxn - i ] / ftau; // AR filter
      asum += 2*flt[i]; 
    }
  hit[0] = flt[0] - asum/fWindow;
  //  std::cout<<">"<<flt[0]<<","<<hit[0]<<"< ";
  // -------------------------------------------------------
  // MA and subtraction in first half window and
  // filter [0.5*MA window, MA window) in half window steps
  for( i=1; i<=fHalfWindow; ++i )
    {
      n = center_window - i;  // index in AR vector
      flt[ i + fHalfWindow - 1 ] = AR[ n ] / ftau; // AR filter
      asum += flt[ i + fHalfWindow - 1 ] - flt[ fHalfWindow - i ];
      hit[i] = flt[i] - asum/fWindow; // MA subtraction
      //      std::cout<<">"<<flt[i]<<","<<hit[i]<<"< ";
    }
  // -------------------------------------------------------
  // MA and subtraction from half window to number of samples minus half window
  // and filter [MA window, N samples) in N samples - 2*MA windows steps
  for( i=fHalfWindow+1; i<=center_window; ++i )
    {
      n = center_window - i; // index in AR vector
      flt[ i + fHalfWindow - 1 ] = AR[ n ] / ftau; // AR filter
      asum += flt[ i + fHalfWindow - 1 ] - flt[ i - fHalfWindow - 1 ];
      hit[i] = flt[i] - asum/fWindow;  // MA subtraction
      //      std::cout<<">"<<flt[i]<<","<<hit[i]<<"< ";
    }
  // -------------------------------------------------------
  // MA and subtraction from number of samples minus half window to number of samples
  // and filter is already done
  for( i=center_window+1; i<nsamples; ++i )
    {
      asum += flt[ nsamples - j++ ] - flt[ i - fHalfWindow - 1 ];
      hit[i] = flt[i] - asum/fWindow;  // MA subtraction
      //      std::cout<<">"<<flt[i]<<","<<hit[i]<<"< ";
    }
  //  std::cout<<"\n";
  // -------------------------------------------------------
}




void FEPfilter(const std::vector<uint16_t>* in,
	       uint16_t& baseline, double& ftau, uint32_t& fWindow,
	       std::vector<double>* out)
{
  // initialize
  size_t nsamples=in->size();
  const uint16_t* adc_samples = in->data();
  //std::cout<<"tau: "<<ftau<<std::endl;
  double _tau = 1./ftau;
  //std::cout<<"1/tau: "<<_tau<<std::endl;
  double _a = 1 - _tau;
  uint32_t fHalfWindow = static_cast<uint32_t>(fWindow/2);
  double dwin = static_cast<double>(fWindow);
      
  // prepare output
  out->resize(nsamples);
  double* hit = out->data(); // AR-MA
      
  // prepare AR kernel
  double AR[nsamples];
  //AR[0] = static_cast<double>(adc_samples[nsamples-1]-baseline);
  AR[0] = static_cast<double>(baseline-adc_samples[nsamples-1]);

  // helper variables
  size_t i, j=1, n; // running index
  double y; // baseline subtracted waveform
  double flt[nsamples]; // AR filtered
  double asum = 0.; // partial sum
  size_t center_window = nsamples - fHalfWindow;
  size_t maxn = nsamples - 1;
      
  // Baseline Subtraction and Auto-Recursive kernel generation
  for( i=1; i<nsamples; ++i)
    {
      //y = static_cast<double>(adc_samples[nsamples-1-i]-baseline);
      y = static_cast<double>(baseline - adc_samples[nsamples-1-i]);
      AR[i] = y + _a*AR[i - 1];
      //      std::cout<<"("<<i<<","<<y<<","<<AR[i]<<") ";
    }
  //std::cout<<"\n";

  // -------------------------------------------------------
  // create output:
  // Auto Recursive filtering and Moving Average Subtraction
  // -------------------------------------------------------
  // MA and subtraction first point and
  // filter [0,0.5*MA window) in half window steps
  for( i=0; i<fHalfWindow; ++i )
    {
      flt[i] = AR[ maxn - i ] * _tau; // AR filter
      asum += 2.*flt[i]; 
    }
  hit[0] = flt[0] - asum/dwin;
  //  std::cout<<">"<<flt[0]<<","<<hit[0]<<"< ";
  // -------------------------------------------------------
  // MA and subtraction in first half window and
  // filter [0.5*MA window, MA window) in half window steps
  for( i=1; i<=fHalfWindow; ++i )
    {
      n = center_window - i;  // index in AR vector
      flt[ i + fHalfWindow - 1 ] = AR[ n ] * _tau; // AR filter
      asum += flt[ i + fHalfWindow - 1 ] - flt[ fHalfWindow - i ];
      hit[i] = flt[i] - asum/dwin; // MA subtraction
      //      std::cout<<">"<<flt[i]<<","<<hit[i]<<"< ";
    }
  // -------------------------------------------------------
  // MA and subtraction from half window to number of samples minus half window
  // and filter [MA window, N samples) in N samples - 2*MA windows steps
  for( i=fHalfWindow+1; i<=center_window; ++i )
    {
      n = center_window - i; // index in AR vector
      flt[ i + fHalfWindow - 1 ] = AR[ n ] * _tau; // AR filter
      asum += flt[ i + fHalfWindow - 1 ] - flt[ i - fHalfWindow - 1 ];
      hit[i] = flt[i] - asum/dwin;  // MA subtraction
      //      std::cout<<">"<<flt[i]<<","<<hit[i]<<"< ";
    }
  // -------------------------------------------------------
  // MA and subtraction from number of samples minus half window to number of samples
  // and filter is already done
  for( i=center_window+1; i<nsamples; ++i )
    {
      asum += flt[ nsamples - j++ ] - flt[ i - fHalfWindow - 1 ];
      hit[i] = flt[i] - asum/dwin;  // MA subtraction
      //      std::cout<<">"<<flt[i]<<","<<hit[i]<<"< ";
    }
  //  std::cout<<"\n";
  // -------------------------------------------------------
}

bool debug = true;
void peak_finderI(const std::vector<int32_t>* in,
		 int32_t& thr, int32_t fMinQA,
		 std::vector<double>& t, std::vector<double>& y)
{
  auto it = in->begin();
  //if( debug ) std::cout<<" --> "<<*std::min_element(in->begin(),in->end())<<std::endl;
  while(true)
    {
      // find where waveform is above thr
      it = std::find_if( it, in->end(),
			 [thr](int32_t const& r)
			 { return (r>thr); } );
      // if it's never above thr, quit
      if( it==in->end() ) break;
	
      uint32_t iStart = std::distance(in->begin(),it);
      if( debug ) std::cout<<"\t["<<iStart;

      // find where waveform goes back below thr
      it = std::find_if( it, in->end(),
			 [thr](int32_t const& r)
			 { return (r<thr); } );
      uint32_t iEnd = std::distance(in->begin(),it);
      if( debug ) std::cout<<" "<<iEnd<<"]";

      // find peak pulse
      std::vector<int32_t>::const_iterator max_pos = std::max_element(in->begin()+iStart,
								      in->begin()+iEnd);
      int32_t A = *max_pos;
      if( debug ) std::cout<<" A: "<<A;
	
      // if the pulse is too 'small'
      int32_t Q = std::accumulate(in->begin()+iStart,in->begin()+iEnd,int32_t(0));
      if( debug ) std::cout<<" Q: "<<Q<<"\tcut: "<<Q/A;
      if( Q/A < fMinQA )
	{
	  ++it;
	  if( debug ) std::cout<<" too small"<<std::endl;
	  continue;
	}

      // peak-width
      uint32_t duration = iEnd-iStart;
      // peak time (sample)
      uint16_t sample = std::distance(in->begin(),max_pos);
      if( debug ) std::cout<<" t: "<<sample;
      t.push_back(sample);
      // peak prominence
      y.push_back(A); 
      
      ++it;
    }
}

void peak_finder(const std::vector<double>* in,
		 double& thr, double fMinQA,
		 std::vector<double>& t, std::vector<double>& y)
{
  auto it = in->begin();
  //if( debug ) std::cout<<" --> "<<*std::min_element(in->begin(),in->end())<<std::endl;
  while(true)
    {
      // find where waveform is above thr
      it = std::find_if( it, in->end(),
			 [thr](double const& r)
			 { return (r>thr); } );
      // if it's never above thr, quit
      if( it==in->end() ) break;
	
      uint32_t iStart = std::distance(in->begin(),it);
      if( debug ) std::cout<<"\t["<<iStart;

      // find where waveform goes back below thr
      it = std::find_if( it, in->end(),
			 [thr](double const& r)
			 { return (r<thr); } );
      uint32_t iEnd = std::distance(in->begin(),it);
      if( debug ) std::cout<<" "<<iEnd<<"]";

      // find peak pulse
      std::vector<double>::const_iterator max_pos = std::max_element(in->begin()+iStart,
								     in->begin()+iEnd);
      double A = *max_pos;
      if( debug ) std::cout<<" A: "<<A;
	
      // if the pulse is too 'small'
      double Q = std::accumulate(in->begin()+iStart,in->begin()+iEnd,double(0));
      if( debug ) std::cout<<" Q: "<<Q<<"\tcut: "<<Q/A;
      if( Q/A < fMinQA )
	{
	  ++it;
	  if( debug ) std::cout<<" too small"<<std::endl;
	  continue;
	}

      // peak-width
      uint32_t duration = iEnd-iStart;
      // peak time (sample)
      double sample = static_cast<double>(std::distance(in->begin(),max_pos));
      if( debug ) std::cout<<" t: "<<sample<<std::endl;
      t.push_back(sample);
      // peak prominence
      y.push_back(A); 
      
      ++it;
    }
}
