/******************************************************************
 *  TTree Writer *
 * 
 * A. Capra
 * August 2021
 *
 ******************************************************************/

#include "manalyzer.h"
#include "midasio.h"

#include "dsvsflow.h"
#include "dsdata.h"

#include <iostream>
#include <fstream>

#include "TTree.h"
#include "TObjString.h"
#include "TError.h"

#include "json.hpp"
using json = nlohmann::json;

class TreeWriterFlags
{
public:
   bool fVerbose;
   std::string fConfigName;
   
   TreeWriterFlags(): fVerbose(false)
   {
      std::string basename=BASEDIRECTORY;
      fConfigName=basename+"/config/master.json";
   }
};

class TreeWriterModule: public TARunObject
{
public:
   TreeWriterFlags* fFlags;
   int fCounter; // counts the number of processed events
   int fError;   // counts the number of encoutered errors

private:
   // channel index
   std::vector<unsigned int>* ch_index;
   std::vector<double>* timestamp;
   // raw signal info
   std::vector<int16_t>* baseline;
   std::vector<float>* baseline_rms;
   std::vector<int16_t>* maxph;
   std::vector<unsigned int>* maxpos;
   std::vector<float>* wf_rms;
   // filtered signal info
   std::vector<int16_t>* filter_baseline;
   std::vector<float>* filter_rms;
   std::vector<int16_t>* filter_maxph;
   std::vector<unsigned int>* filter_maxpos;
   std::vector<float>* filterwf_rms;
   // channel info tree
   TTree *ChanTree;
   
   int fNhit;
   std::vector<uint16_t>*  fChannelIndex;
   std::vector<uint16_t>*  fAmplitude;
   std::vector<uint16_t>*  fTime;
   //   std::vector<uint32_t>*  fTimeOverThreshold;
   std::vector<double>*    fHitTimeStamp;
   TTree *HitTree;

   double fmidasts;
   int fevent_number;
   unsigned int fslice_index;
   double fslice_start; 

public:
   TreeWriterModule(TARunInfo* runinfo, TreeWriterFlags* f): TARunObject(runinfo),
							     fFlags(f),
							     fCounter(0), fError(0),
                                                             ChanTree(0),
                                                             fNhit(0), HitTree(0)
   {
      if(fFlags->fVerbose) std::cout<<"TreeWriterModule ctor"<<std::endl;
#ifdef HAVE_MANALYZER_PROFILER
      fModuleName="TreeWriter";
#endif  
      ch_index     = new std::vector<unsigned int>();
      timestamp    = new std::vector<double>();
      baseline     = new std::vector<int16_t>();
      baseline_rms = new std::vector<float>();
      maxph        = new std::vector<int16_t>();
      maxpos       = new std::vector<unsigned int>();
      wf_rms       = new std::vector<float>();

      fChannelIndex      = new std::vector<uint16_t>();
      fAmplitude         = new std::vector<uint16_t>();
      fTime              = new std::vector<uint16_t>();
      //      fTimeOverThreshold = new std::vector<uint32_t>();
      fHitTimeStamp      = new std::vector<double>();
   }


   void BeginRun(TARunInfo* runinfo)
   {
      if(runinfo->fFileName.empty() )
         std::cout<<"TreeWriterModule::BeginRun() run: "<<runinfo->fRunNo<<" online"<<std::endl;
      else
         std::cout<<"TreeWriterModule::BeginRun() run: "<<runinfo->fRunNo<<" midasfile: "<<runinfo->fFileName.c_str()<<std::endl;

      runinfo->fRoot->fOutputFile->cd(); // select correct ROOT directory

      ChanTree = new TTree("TChan","Darkside VSlice Channels");
      ChanTree->Branch("midas_ts",    &fmidasts);      // unix epoch
      ChanTree->Branch("midas_sn",    &fevent_number); // midas event serial number
      ChanTree->Branch("slice_index", &fslice_index);  // slice index
      ChanTree->Branch("slice_start", &fslice_start);  // slice start in sec since BOR
      ChanTree->Branch("ch_index",    &ch_index);
      ChanTree->Branch("timestamp",   &timestamp);
      ChanTree->Branch("baseline",    &baseline);
      ChanTree->Branch("baseline_rms",&baseline_rms);
      ChanTree->Branch("maxph",       &maxph);
      ChanTree->Branch("maxpos",      &maxpos);
      ChanTree->Branch("wf_rms",      &wf_rms);
                       

      HitTree = new TTree("dsvstree","Darkside Hits");
      HitTree->Branch("midas_ts",    &fmidasts);      // unix epoch
      HitTree->Branch("midas_sn",    &fevent_number); // midas event serial number
      HitTree->Branch("slice_index", &fslice_index);  // slice index
      HitTree->Branch("slice_start", &fslice_start);  // slice start in sec since BOR
      HitTree->Branch("index",       &fChannelIndex); // index = ADC# x chans per ADC + CH#
      //      HitTree->Branch("charge",&fCharge); // integral
      HitTree->Branch("time",        &fTime); // number of samples
      HitTree->Branch("height",      &fAmplitude); // ADC
      //      HitTree->Branch("tot",         &fTimeOverThreshold);// time over threshold in samples
      HitTree->Branch("ts",          &fHitTimeStamp);
      HitTree->Branch("nhit",        &fNhit); // number of hits found in the event

      std::ifstream fin(fFlags->fConfigName.c_str());
      json settings;
      fin>>settings;
      if(fFlags->fVerbose)
         std::cout<<"TreeWriterModule Json parsing success!"<<std::endl;
      fin.close();

      std::cout<<"TreeWriterModule::BeginRun Saving settings to rootfile... ";
      runinfo->fRoot->fOutputFile->cd(); // select correct ROOT directory
      int error_level_save = gErrorIgnoreLevel;
      gErrorIgnoreLevel = kFatal;
      TObjString settings_obj(settings.dump().c_str());
      int bytes_written = gDirectory->WriteTObject(&settings_obj,"config");
      if( bytes_written > 0 )
         std::cout<<" DONE ("<<bytes_written<<")"<<std::endl;
      else
         std::cout<<" FAILED"<<std::endl;
      gErrorIgnoreLevel = error_level_save;

      uint32_t midas_start_time;
      runinfo->fOdb->RU32("Runinfo/Start time binary",(uint32_t*) &midas_start_time);
      time_t ts1 = midas_start_time;
      std::string string_time=std::to_string(ts1);
      std::cout<<"TreeWriterModule::BeginRun Saving start time to rootfile... ";
      runinfo->fRoot->fOutputFile->cd(); // select correct ROOT directory
      error_level_save = gErrorIgnoreLevel;
      gErrorIgnoreLevel = kFatal;
      TObjString string_time_obj(string_time.c_str());
      bytes_written = gDirectory->WriteTObject(&string_time_obj,"BeginRunUnixTS");
      if( bytes_written > 0 )
         std::cout<<" DONE ("<<bytes_written<<")"<<std::endl;
      else
         std::cout<<" FAILED"<<std::endl;
      gErrorIgnoreLevel = error_level_save;
      
   }

   void EndRun(TARunInfo* runinfo)
   {
      uint32_t midas_end_time;
      runinfo->fOdb->RU32("Runinfo/Stop time binary",(uint32_t*) &midas_end_time);
      time_t ts1 = midas_end_time;
      std::string string_time=std::to_string(ts1);
      std::cout<<"TreeWriterModule::EndRun Saving end time to rootfile... ";
      runinfo->fRoot->fOutputFile->cd(); // select correct ROOT directory
      int error_level_save = gErrorIgnoreLevel;
      gErrorIgnoreLevel = kFatal;
      TObjString string_time_obj(string_time.c_str());
      int bytes_written = gDirectory->WriteTObject(&string_time_obj,"EndRunUnixTS");
      if( bytes_written > 0 )
         std::cout<<" DONE ("<<bytes_written<<")"<<std::endl;
      else
         std::cout<<" FAILED"<<std::endl;
      gErrorIgnoreLevel = error_level_save;
      std::cout<<"TreeWriterModule::EndRun() run: "<<runinfo->fRunNo
               <<" events: "<<fCounter<<" errors: "<<fError<<std::endl;
   }

  
   TAFlowEvent* AnalyzeFlowEvent(TARunInfo* /*runinfo*/, TAFlags* flags, TAFlowEvent* flow)
   {
#ifdef MODULE_MULTITHREAD
      std::lock_guard<std::mutex> lock(TAMultithreadHelper::gfLock);
#endif
      
      DSProcessorFlow* wf_flow = flow->Find<DSProcessorFlow>();
      if( !wf_flow ) 
         {
#ifdef HAVE_MANALYZER_PROFILER
            *flags|=TAFlag_SKIP_PROFILE;
#endif
            ++fError;
            return flow;
         }
      
      fmidasts=wf_flow->fEventTS;
      fevent_number=wf_flow->fEventNumber;
      fslice_index=wf_flow->fSliceIdx;
      fslice_start=wf_flow->fSliceStart;

      ch_index->clear();
      timestamp->clear();
      baseline->clear();
      baseline_rms->clear();
      maxph->clear();
      maxpos->clear();
      wf_rms->clear();
      
      //if(fFlags->fVerbose)
      std::cout<<"TreeWriterModule::AnalyzeFlowEvent Event: "<<wf_flow->fEventNumber
               <<" Writing "<<wf_flow->GetNumberOfChannels()
               <<" channels for slice "<<wf_flow->fSliceIdx<<std::endl;
      
      for( size_t i=0; i<wf_flow->GetNumberOfChannels(); ++i )
         {
            TDSChannel* ch = wf_flow->GetDSchan(i);
            for(size_t j=0; j<ch->timestamp.size(); ++j)
               {
                  ch_index->push_back( ch->index );
                  timestamp->push_back( ch->timestamp.at(j) );
                  baseline->push_back( ch->baseline.at(j) );
                  baseline_rms->push_back( ch->baseline_rms.at(j) );
                  maxph->push_back( ch->maxph.at(j) );
                  maxpos->push_back( ch->maxpos.at(j) );
                  wf_rms->push_back( ch->wf_rms.at(j) );
               }
            //            std::cout<<ch->index<<" "<<ch->maxph.front()<<" "<<ch->wf_rms.front()<<std::endl;
         }
      ChanTree->Fill();

      //if(fFlags->fVerbose)
      std::cout<<"TreeWriterModule::AnalyzeFlowEvent Writing "
               <<wf_flow->GetNumberOfHits()<<" hits"<<std::endl;
      fNhit=0;
      fChannelIndex->clear();
      //      fCharge.clear();
      fTime->clear();
      fAmplitude->clear();
      //      fTimeOverThreshold->clear();
      fHitTimeStamp->clear();
      for( size_t i=0; i<wf_flow->GetNumberOfHits(); ++i)
        {
           const DSVSHit* dsp =wf_flow->GetHit(i);
           if( fFlags->fVerbose && 0 )
              dsp->Print();
           fChannelIndex->push_back(dsp->index);
           //           fCharge.push_back(dsp->charge);
           fTime->push_back(dsp->time);
           fAmplitude->push_back(dsp->height);
           //           fTimeOverThreshold->push_back(dsp->tot);
           fHitTimeStamp->push_back(dsp->timestamp);
           ++fNhit;
         }
       HitTree->Fill();
      
      ++fCounter;
      return flow;
   }
};


class TreeWriterModuleFactory: public TAFactory
{
public:
   TreeWriterFlags fFlags;
   
public:
   void Init(const std::vector<std::string> &args)
   {
      printf("TreeWriterModuleFactory::Init!\n");
      
      for (unsigned i=0; i<args.size(); i++)
         {
            if( args[i] == "--verbose" ||
                args[i] == "-v" )
               fFlags.fVerbose = true;
            if( args[i] == "--config" ||
                args[i] == "--conf" ||
                args[i] == "-c" )
               fFlags.fConfigName=args[i+1];
	 }
   }
   
   void Finish()
   {
      printf("TreeWriterModuleFactory::Finish!\n");
   }
   
   TARunObject* NewRunObject(TARunInfo* runinfo)
   {
      printf("TreeWriterModuleFactory::NewRunObject, run %d, file %s\n", runinfo->fRunNo, runinfo->fFileName.c_str());
      return new TreeWriterModule(runinfo, &fFlags);
   }

};

static TARegister tar(new TreeWriterModuleFactory);

/* emacs
 * Local Variables:
 * tab-width: 8
 * c-basic-offset: 3
 * indent-tabs-mode: nil
 * End:
 */
