from sys import argv
import uproot
import awkward as ak
import matplotlib.pyplot as plt

with uproot.open(argv[1]) as fin: # open root file

    tree = fin["TChan"] # get TTree "TChan"
    tree.show() # inspect TChan

    # prepare plots
    fig, ax = plt.subplots(figsize=(13,9))

    ch = tree["ch_index"].array()
    ph = tree["maxph"].array()
    ts = tree["timestamp"].array()
    chan = list(range(0,8*64,64))
    # loop over slices per channels, timestamps and pulse heights
    for n, si in enumerate(tree["slice_index"].array()):
        for c in chan:
            ch0=ak.where(ch[n]==c) 
            ax.plot(ts[n][ch0],ph[n][ch0],'.',label=f'Slice {si} CH: {c}')
    # end of slices loop

    ax.set_title("CH0 Pulse Height vs Time")
    ax.set_xlabel("Timestamp since the beginning of the Slice [s]")
    ax.set_ylabel("Pulse Height")
    _,ymax=ax.set_ylim()
    ax.set_ylim(0,ymax)
    ax.legend()
    fig.tight_layout()
    plt.show()
    
