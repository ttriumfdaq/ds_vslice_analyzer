from sys import argv
import uproot
import awkward as ak
import matplotlib.pyplot as plt


def plot_hits(tree,ax,ch=0):
    # get branches as arrays
    ss=tree["slice_start"].array()
    si=tree["slice_index"].array()
    chans=tree["index"].array()
    t=tree["time"].array()
    ts=tree["ts"].array()
    Dt=tree["tot"].array()
    k=tree["height"].array()
    
    # loop over slices per channels, timestamps and pulse heights
    for n in range(ak.num(si,axis=0)):
        print(f"{n} --> Slice Start {ss[n]} s -- Slice Index {si[n]}")

        # select channel 0
        ch0=ak.where(chans[n]==ch)
        
        # convert samples to ns
        # add the global timestamp
        # subtract the slice (start) timestamp
        pt=t[n][ch0]*8e-9 + ts[n][ch0] - ss[n]
        
        # pulse height
        pk=k[n][ch0]
        print(f"Number of hits in channel {ch} is {ak.size(pk)}")

        # time over threshold
        Dt0=Dt[n][ch0]*8e-9

        ax[0,0].plot(pt,pk,'.',label=f'Slice {si[n]}')
        ax[0,1].hist(pk,bins=1000,label=f'Slice {si[n]}')
        ax[1,0].hist(Dt0,bins=50,label=f'Slice {si[n]}')
        ax[1,1].plot(pk,Dt0,'.',label=f'Slice {si[n]}')
    # end of slices loop


def get_tree(fname):
    fin=uproot.open(fname) # open ROOT file
    tree = fin["dsvstree"] # get TTree
    #tree.show() # inspect tree
    return tree


if __name__ == "__main__":

    # prepare plots
    fig, ax = plt.subplots(ncols=2,nrows=2,figsize=(11,7),num="dsvstree")

    for f in argv[1:]:
        tree = get_tree(f)
        plot_hits(tree,ax)

    ax[0,0].set_title("CH0 Pulse Height vs Time")
    ax[0,0].set_xlabel("Timestamp since the beginning of the Slice [s]")
    ax[0,0].set_ylabel("Pulse Height")
    ax[0,0].legend()

    ax[0,1].set_title("CH0 Prominence")
    ax[0,1].set_xlabel("Height [a.u.]")

    ax[1,0].set_title("CH0 Pulse Duration")
    ax[1,0].set_xlabel("Time [s]")

    ax[1,1].set_title("CH0 Pulse Height vs Duration")
    ax[1,1].set_xlabel("Pulse Height")
    ax[1,1].set_xlabel("Pulse Duration [s]")
    
    fig.tight_layout()
    plt.show()
    
