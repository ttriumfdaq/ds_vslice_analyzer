/******************************************************************
 *  Hit finder *
 * 
 * A. Capra
 * August 2021
 *
 ******************************************************************/

#include "manalyzer.h"
#include "midasio.h"

#include "dsvsflow.h"
#include "dsdata.h"

#include <iostream>
#include <fstream>
#include <cmath>
#include <vector>
#include <algorithm>
#include <iterator>
#include <numeric>
#include <cassert>


#include "hitfinder.h"


#include "json.hpp"
using json = nlohmann::json;


class HitFinderFlags
{
public:
   bool fVerbose;
   std::string fConfigName;
   
   HitFinderFlags():fVerbose(false)
   {
      std::string basename=BASEDIRECTORY;
      fConfigName=basename+"/config/master.json";
   }
};


class HitFinderModule: public TARunObject
{
public:
   HitFinderFlags* fFlags;
   int fCounter; // counts the number of processed events
   int fError;   // counts the number of encoutered errors

private:
   unsigned fNtotPulses;
   unsigned fNHits;

   int32_t fAmplitude;
   uint32_t fMAgate;
   int32_t fMinCharge;

   HitFinder hf;

public:
   HitFinderModule(TARunInfo* runinfo, HitFinderFlags* f): TARunObject(runinfo),
                                                           fFlags(f), fCounter(0), fError(0),
                                                           fNtotPulses(0), fNHits(0)
   {
      if(fFlags->fVerbose) std::cout<<"HitFinderModule ctor"<<std::endl;
#ifdef HAVE_MANALYZER_PROFILER
      fModuleName="HitFinder";
#endif

      std::ifstream fin(fFlags->fConfigName.c_str());
      json settings;
      fin>>settings;
      if(fFlags->fVerbose)
         std::cout<<"HitFinderModule Json parsing success!"<<std::endl;
      fin.close();
      
      fMAgate=settings["Pulse"]["MAgate"].get<uint32_t>();
      std::cout<<"HitFinderModule MA gate: "<<fMAgate<<std::endl;
      fAmplitude=settings["Pulse"]["Amplitude"].get<int32_t>();
      std::cout<<"HitFinderModule threshold: "<<fAmplitude<<std::endl;
      fMinCharge=settings["Pulse"]["MinCharge"].get<int32_t>();
      std::cout<<"HitFinderModule Charge/Prominence Cut: "<<fMinCharge<<std::endl;
 
      hf = HitFinder(fMAgate,fAmplitude,fMinCharge);
   }


   void BeginRun(TARunInfo* runinfo)
   {
      if(runinfo->fFileName.empty() )
         std::cout<<"HitFinderModule::BeginRun() run: "<<runinfo->fRunNo<<" online"<<std::endl;
      else
         std::cout<<"HitFinderModule::BeginRun() run: "<<runinfo->fRunNo<<" midasfile: "<<runinfo->fFileName.c_str()<<std::endl;
   }

   void EndRun(TARunInfo* runinfo)
   {
      std::cout<<"HitFinderModule::EndRun() run: "<<runinfo->fRunNo<<" events: "<<fCounter<<" errors: "<<fError<<"\tTotal number of Hits: "<<fNtotPulses<<std::endl;
   }

  
   TAFlowEvent* AnalyzeFlowEvent(TARunInfo* /*runinfo*/, TAFlags* flags, TAFlowEvent* flow)
   {
      DSProcessorFlow* wf_flow = flow->Find<DSProcessorFlow>();
      if( !wf_flow ) 
         {
            ++fError;
#ifdef HAVE_MANALYZER_PROFILER
            *flags|=TAFlag_SKIP_PROFILE;
#endif
            return flow;
         }

      //if( fFlags->fVerbose )
      std::cout<<"HitFinderModule::AnalyzeFlowEvent # of ch: "<<wf_flow->GetNumberOfChannels()
               <<" in slice index: "<<wf_flow->fSliceIdx<<std::endl;

       for( size_t ich=0; ich<wf_flow->GetNumberOfChannels(); ++ich )
         {
            TDSChannel* aCh = wf_flow->GetDSchan(ich);
            if( fFlags->fVerbose && 0 ) aCh->Print();

            assert(aCh->filtered_wf.size() == aCh->timestamp.size() );
            unsigned idx=0;
            for( auto& wf: aCh->filtered_wf )
               {
                  hf(&wf);
                  fNHits += hf.GetNumberOfHits();
                  for(size_t i=0; i<hf.GetNumberOfHits(); ++i)
                     {
                        DSVSHit* aHit = new DSVSHit(aCh->module,aCh->channel);
                        aHit->time=hf.Gett(i);
                        aHit->height=hf.Getk(i);
                        //aHit->tot=hf.GetDt(i);
                        aHit->timestamp=aCh->timestamp[idx];
                        if( fFlags->fVerbose && 0 ) aHit->Print();
                        wf_flow->hits->push_back(aHit);
                     } // loop over hits
                  fNtotPulses += hf.GetNumberOfHits();
                  aCh->MAsub_wf.push_back( *hf.GetWF() );
                  hf.Reset();
                  ++idx;
               }// loop over waveforms
         }// loop over channels
       
      //if( fFlags->fVerbose )
      std::cout<<"HitFinderModule::AnalyzeFlowEvent # "<<fCounter
               <<"\t number of hits: "<<wf_flow->GetNumberOfHits()<<" "<<fNHits<<std::endl;
      
      flow=wf_flow;
      ++fCounter;
      return flow;
   }
};


class HitFinderModuleFactory: public TAFactory
{
public:
   HitFinderFlags fFlags;
   
public:
   void Init(const std::vector<std::string> &args)
   {
      printf("HitFinderModuleFactory::Init!\n");
      
      for (unsigned i=0; i<args.size(); i++)
         {
            if( args[i] == "--verbose" ||
                args[i] == "-v" )
               fFlags.fVerbose = true;
            if( args[i] == "--config" ||
                args[i] == "--conf" ||
                args[i] == "-c" )
               fFlags.fConfigName=args[i+1];
         }
   }
   
   void Finish()
   {
      printf("HitFinderModuleFactory::Finish!\n");
   }
   
   TARunObject* NewRunObject(TARunInfo* runinfo)
   {
      printf("HitFinderModuleFactory::NewRunObject, run %d, file %s\n", runinfo->fRunNo, runinfo->fFileName.c_str());
      return new HitFinderModule(runinfo, &fFlags);
   }

};

static TARegister tar(new HitFinderModuleFactory);

/* emacs
 * Local Variables:
 * tab-width: 8
 * c-basic-offset: 3
 * indent-tabs-mode: nil
 * End:
 */
