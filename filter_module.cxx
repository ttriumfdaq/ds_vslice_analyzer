/******************************************************************
 *  Basic Filerting *
 * 
 * A. Capra
 * August 2021
 * Updated: March 2022
 *
 ******************************************************************/

#include "manalyzer.h"
#include "midasio.h"

#include "dsvsflow.h"

#include <iostream>
#include <fstream>
#include <cmath>

#include <cassert>

#include "filter.h"

#include "json.hpp"
using json = nlohmann::json;

class FilterFlags
{
public:
   bool fVerbose;
   std::string fConfigName;
   FilterFlags():fVerbose(false)
   {
      std::string basename=BASEDIRECTORY;
      fConfigName=basename+"/config/master.json";
   }
};
  

class FilterModule: public TARunObject
{
public:
   FilterFlags* fFlags;

private:
   int fCounter; // counts the number of processed events
   int fError;   // counts the number of encountered errors

   int fWindow=100;
   double fSmoothing=0.88;
   bool fMAF=false;
   bool fEXF=false;

   // ARMA filter 
   bool fARMA=false;
   double fARMAtau=80.;
   double fARMAsigma=2.;
   double fARMAexp=1.;

   // AR filter
   bool fARF=false;

   // Filter classes
   ARMA armafilter;
   ARF arfilter;

public:
   FilterModule(TARunInfo* runinfo, FilterFlags* f): TARunObject(runinfo),
                                                     fFlags(f), fCounter(0), fError(0)
   {
      if(fFlags->fVerbose) std::cout<<"FilterModule ctor"<<std::endl;
#ifdef HAVE_MANALYZER_PROFILER
      fModuleName="Filter";
#endif

      std::cout<<"FilterModule Json file: "<<fFlags->fConfigName<<std::endl;
      std::ifstream fin(fFlags->fConfigName.c_str());
      json settings;
      fin>>settings;
      if(fFlags->fVerbose)
         std::cout<<"FilterModule Json parsing success!"<<std::endl;
      fin.close();

      std::string filter_name = settings["Filter"]["Name"].get<std::string>();
      if( 0 ) std::cout<<"do nothing"<<std::endl;
      else if( filter_name == "ARMA" )
         {
            fARMA=true;
            fARMAtau=settings["Filter"]["Par0"].get<double>();
            fARMAsigma=settings["Filter"]["Par1"].get<double>();
            fARMAexp=settings["Filter"]["Par2"].get<double>();
            std::cout<<filter_name<<" filter tau: "<<fARMAtau
                     <<" sigma_fp: "<<fARMAsigma<<" Afp_Aexp_ratio: "<<fARMAexp<<std::endl;
            armafilter = ARMA( fARMAtau, fARMAsigma, fARMAexp );
         }
      else if( filter_name == "AR" )
         {
            fARF=true;
            fARMAtau=settings["Filter"]["Par0"].get<double>();
            std::cout<<filter_name<<" filter tau: "<<fARMAtau<<std::endl;
            arfilter = ARF( fARMAtau );
         }
      else
         {
            std::cerr<<"Unkwown filter "<<filter_name<<std::endl;
         }
   }


   void BeginRun(TARunInfo* runinfo)
   {
      if(runinfo->fFileName.empty() )
         std::cout<<"FilterModule::BeginRun() run: "<<runinfo->fRunNo<<" online"<<std::endl;
      else
         std::cout<<"FilterModule::BeginRun() run: "<<runinfo->fRunNo<<" midasfile: "<<runinfo->fFileName.c_str()<<std::endl;
   }

   void EndRun(TARunInfo* runinfo)
   {
      std::cout<<"FilterModule::EndRun() run: "<<runinfo->fRunNo<<" events: "<<fCounter<<" errors: "<<fError<<std::endl;
   }

  
   TAFlowEvent* AnalyzeFlowEvent(TARunInfo* /*runinfo*/, TAFlags* flags, TAFlowEvent* flow)
   {
      std::cout<<"FilterModule::AnalyzeFlowEvent # "<<fCounter<<std::endl;
      DSProcessorFlow* wf_flow = flow->Find<DSProcessorFlow>();
      if( !wf_flow ) 
         {
            ++fError;
#ifdef HAVE_MANALYZER_PROFILER
            *flags|=TAFlag_SKIP_PROFILE;
#endif
            return flow;
         }

      //      if( fFlags->fVerbose )
      std::cout<<"FilterModule::AnalyzeFlowEvent # of Channels: "<<wf_flow->GetNumberOfChannels()<<std::endl;
      // Loop over channels
      for( size_t ich=0; ich<wf_flow->GetNumberOfChannels(); ++ich )
         {
            TDSChannel* aCh = wf_flow->GetDSchan(ich);
            if( fFlags->fVerbose && 0 ) aCh->Print();
            for( auto& wf: aCh->unfiltered_wf )
               {
                  if( 0 ) std::cout<<"do nothing"<<std::endl;
                  else if( fARMA )
                     {
                        aCh->filtered_wf.push_back( *armafilter( &wf ) );
                     }
                  else if( fARF )
                     {
                        aCh->filtered_wf.push_back( *arfilter( &wf ) );
                     } 
                  else
                     {
                        if( fFlags->fVerbose )
                           std::cout<<"FilterModule::AnalyzeFlowEvent WARNING: the filtered waveform will be unfiltered"<<std::endl;
                     }
               }
         } // end of loop over channels
      ++fCounter;
      return flow;
   }
};


   class FilterModuleFactory: public TAFactory
   {
   public:
      FilterFlags fFlags;
   
   public:

      void Usage()
      {
         //      printf("Modules options flags:\n");
         printf("\n");
         printf("\t--config, --conf, -c </path/to/json_file>\n\t\tspecify path json configuration file\n");
         //      printf("\t--verbose, -v\n\t\tprint status information\n");
         //      printf("\t--skip-events, -s <number>\n\t\tskip to event <number> (starting from 0)\n");
      }
      void Init(const std::vector<std::string> &args)
      {
         printf("FilterModuleFactory::Init!\n");
      
         for (unsigned i=0; i<args.size(); i++)
            {
               if( args[i] == "--verbose" ||
                   args[i] == "-v" )
                  fFlags.fVerbose = true;
               if( args[i] == "--config" ||
                   args[i] == "--conf" ||
                   args[i] == "-c" )
                  fFlags.fConfigName=args[i+1];
            }
      
      }
   
      void Finish()
      {
         printf("FilterModuleFactory::Finish!\n");
      }
   
      TARunObject* NewRunObject(TARunInfo* runinfo)
      {
         printf("FilterModuleFactory::NewRunObject, run %d, file %s\n", runinfo->fRunNo, runinfo->fFileName.c_str());
         return new FilterModule(runinfo, &fFlags);
      }

   };

   static TARegister tar(new FilterModuleFactory);

   /* emacs
    * Local Variables:
    * tab-width: 8
    * c-basic-offset: 3
    * indent-tabs-mode: nil
    * End:
    */
