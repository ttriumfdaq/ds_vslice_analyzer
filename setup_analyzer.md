# System

```
$ uname -n
dsvslice.triumf.ca
$ uname -v
\#64~18.04.1-Ubuntu SMP Wed Dec 9 17:11:11 UTC 2020
$ gcc --version | head -1
gcc (Ubuntu 7.5.0-3ubuntu1~18.04) 7.5.0
$ python -V
Python 3.8.10
$ git --version
git version 2.17.1
$ cmake --version | head -1
cmake version 3.10.2
```

The `cmake` version is too old


# Configure personal workspace

```
export WORKSPACE=$HOME/andrea
```


# Build cmake 3.21

```
wget https://github.com/Kitware/CMake/releases/download/v3.21.0/cmake-3.21.0.tar.gz
tar xzvf cmake-3.21.0.tar.gz
cd cmake-3.21.0
cmake .
make
```

# Build libxml2

On this machine `libxml2-dev` is not installed by default

```
git clone https://gitlab.gnome.org/GNOME/libxml2
cd libxml2
mkdir -p libxml2 
./autogen.sh --prefix=$WORKSPACE/libxml2/libxml2
make
make install
```

# Build root

Getting the latest version

```
git clone --branch latest-stable https://github.com/root-project/root.git root_src
mkdir -p root_build root
cd root_build
$WORKSPACE/cmake-3.21.0/bin/cmake -DCMAKE_BUILD_TYPE=Release -DCMAKE_INSTALL_PREFIX=$WORKSPACE/root -Dminuit2=ON -DLIBXML2_LIBRARY=$WORKSPACE/libxml2/libxml2/lib/libxml2.so -DLIBXML2_INCLUDE_DIR=$WORKSPACE/libxml2/libxml2/include/libxml2 -Dxml=ON -Dhttp=ON -Dmathmore=ON -Dx11=ON -Dxrootd=OFF ../root_src/
$WORKSPACE/cmake-3.21.0/bin/cmake --build . --target install -- -j`nproc`
source $WORKSPACE/root/bin/thisroot.sh
```

# Build midas

Getting the latest version

```
git clone https://bitbucket.org/tmidas/midas.git --recursive
cd midas
mkdir -p build
cd build
$WORKSPACE/cmake-3.21.0/bin/cmake ..
$WORKSPACE/cmake-3.21.0/bin/cmake --build . --target install -- -j`nproc`
export MIDASSYS=$WORKSPACE/midas
```
