// *******************************************************
// Analyzer module for TRIUMF DarkSide Vertical Slice
// This module fills histograms relevant to the TSP
//
// A. Capra - TRIUMF
// July 2021
//
// *******************************************************

#include <iostream>
#include <vector>
#include <map>
#include <numeric>

#include "manalyzer.h"

#include "dsvsflow.h" // data to pass to other modules

#include "TH1D.h"
#include "TH2D.h"

class TSPhistoFlags
{
public:
   bool fVerb=false;
   bool fEnabled=false;
};


class TSPhistoModule: public TARunObject
{
private:
   TSPhistoFlags* fFlags;
   
   // Slices histograms
   TH2D* hsliceidxtsp; // TSP index vs slice index
   TH2D* hmts_slicestart; // midas time vs slice time start
   TH2D* hslice_idx_start; // slice index vs slice time start

   // Pulses (channels) histograms
   TH2D* hsliceidx_chan; // number of channels in each slice
   TH2D* hslicestart_pulsetime; // slice time start vs pulse timestamp
   TH1D* hocc; // occupancy per channel
   TH1D* hFEP; // occupancy per FEP
   TH1D* hts; // pulse timestamp in sec
   TH2D* htsocc; // pulse per channel vs time
   TH2D* htsFEP; // pulse per FEP vs time

   std::map<int,TH2D*> fmapslice_htsocc;
   std::map<int,TH2D*> fmapslice_htsFEP;

   std::map<int,std::map<int,TH2D*>> fmapslice_mapFEP_htsocc;

   // WAVEFORM HISTOs
   std::map<int,TH2D*> fmapslice_hphchan;
   std::map<int,std::map<int,TH2D*>> fmapslice_mapchan_htsph;

   double mtstart=1.62e9, mtstop=1.63e9;
   double ststart=0., ststop=100.;
   int stbin=100;

   unsigned int NFEP=4; // these will change
   unsigned int NADC=8;
   
public:
   TSPhistoModule(TARunInfo* runinfo, TSPhistoFlags* flags):
      TARunObject(runinfo), fFlags(flags)
   {
      if(fFlags->fVerb) std::cout<<"TSPhistoModule ctor"<<std::endl;
#ifdef HAVE_MANALYZER_PROFILER
      fModuleName="TSPhisto";
#endif
      
   }

   ~TSPhistoModule()
   {
      if(fFlags->fVerb) std::cout<<"TSPhistoModule dtor"<<std::endl;
   }

   void BeginRun(TARunInfo* runinfo)
   {
      if( !fFlags->fEnabled ) return;
      std::cout<<"TSPhistoModule::BeginRun()"<<std::endl;

      uint32_t midas_start_time;
      runinfo->fOdb->RU32("Runinfo/Start time binary",(uint32_t*) &midas_start_time);
      time_t ts1 = midas_start_time;
      mtstart = (double) ts1;
      uint32_t midas_end_time;
      runinfo->fOdb->RU32("Runinfo/Stop time binary",(uint32_t*) &midas_end_time);
      time_t ts2 = midas_end_time;
      mtstop = (double) ts2;
      
      runinfo->fRoot->fOutputFile->cd(); // select the correct file
      gDirectory->mkdir("tsphisto")->cd();
      
      hsliceidxtsp = new TH2D("hsliceidxtsp","TSP index Vs. Slice index;Slice index;TSP index",5,0.5,5.5,100,0.5,100.5);
      hsliceidxtsp->SetMarkerStyle(8); hsliceidxtsp->SetMarkerColor(kBlack);
      
      hmts_slicestart = new TH2D("hmts_slicestart","MIDAS time vs Slice start;MIDAS time stamp [s];Slice start time [s]",
				 100,mtstart,mtstop,
				 stbin,ststart,ststop);
      hmts_slicestart->SetMarkerStyle(8);  hmts_slicestart->SetMarkerColor(kBlack);

      hslice_idx_start = new TH2D("hslice_idx_start","TSP Slice Index vs Time;Slice index;Slice start time [s]",100,0.5,100.5,stbin,ststart,ststop);
      hslice_idx_start->SetMarkerStyle(8); hslice_idx_start->SetMarkerColor(kBlack);

      hsliceidx_chan = new TH2D("hsliceidx_chan","Number of Channels per Slice;Slice index;",100,0.5,100.,64*NADC,-0.5,64.*NADC-0.5);
      hsliceidx_chan->SetMarkerStyle(8);  hsliceidx_chan->SetMarkerColor(kBlack);
      
      hocc = new TH1D("hocc","Number of pulses per Channel;Channel Index = number+64*board",64*NADC,-0.5,64.*NADC-0.5);
      hocc->SetMinimum(0);

      hFEP = new TH1D("hFEP","Number of pulses per FEP;FEP number",NFEP,0.5,NFEP+0.5);
      hFEP->SetMinimum(0);
      
      hts = new TH1D("hts","Pulse Timestamp since BOR;Time [s]",1000,ststart,ststop);

      hslicestart_pulsetime = new TH2D("hslicestart_pulsetime",
                                       "Slice start Vs Pulse time;Slice start time [s];Pulse time [s]",
                                       stbin,ststart,ststop,
                                       1000,ststart,ststop);
      //      hslicestart_pulsetime->SetMarkerStyle(8);  hslicestart_pulsetime->SetMarkerColor(kBlack);

      htsocc = new TH2D("htsocc","Pulse per Channel Vs Time;Pulse Time [s];Channel Index = number+64*board",1000,ststart,ststop,64*NADC,-0.5,64.*NADC-0.5);
      htsFEP = new TH2D("htsFEP","Pulse per FEP Vs Time;Pulse Time [s];FEP number",1000,ststart,ststop,NFEP,0.5,NFEP+0.5);
   }

   void EndRun(TARunInfo* /*runinfo*/)
   {
      std::cout<<"TSPhistoModule::EndRun()"<<std::endl;
   }

   void FillHisto(TARunInfo* runinfo, std::vector<SlicePulse*>* pulses, double& mts)
   {
#ifdef MODULE_MULTITHREAD
      std::lock_guard<std::mutex> lock(TAMultithreadHelper::gfLock);
#endif
      //hnpulses->Fill( (double) pulses->size() );
      for(auto it=pulses->begin(); it!=pulses->end(); ++it)
         {
            SlicePulse* aPulse = *it; // get pointer to SlicePulse obj
            
            double itsp=double(aPulse->tspid); // TSP index as double
            double islc=double(aPulse->sid); // slice index as double
            hsliceidxtsp->Fill(islc,itsp);
            hmts_slicestart->Fill(mts,aPulse->sts);
            hslice_idx_start->Fill(islc,aPulse->sts);

            // Pulses (channels) histograms
            double index = double(aPulse->GetIndex());
            hocc->Fill(index);
            hFEP->Fill(aPulse->fepid);
            hts->Fill(aPulse->wfts);

            hsliceidx_chan->Fill(islc, index);
            hslicestart_pulsetime->Fill(aPulse->sts,aPulse->wfts);

            htsocc->Fill(aPulse->wfts,index);
            htsFEP->Fill(aPulse->wfts,aPulse->fepid);
            
            runinfo->fRoot->fOutputFile->cd(); // select the correct file
            if( !runinfo->fRoot->fOutputFile->FindObject("slicemap_chan") )
               runinfo->fRoot->fOutputFile->mkdir("slicemap_chan");
            runinfo->fRoot->fOutputFile->cd("slicemap_chan");
            if( !fmapslice_htsocc.count(aPulse->sid) )
               {
                  std::string hname="htsocc_slice"; hname+=std::to_string(aPulse->sid);
                  std::string htitle="Pulse per Channel Vs Time  -- Slice # "+std::to_string(aPulse->sid)+";Pulse Time [s];Channel Index = number+64*board";
                  fmapslice_htsocc[aPulse->sid] = new TH2D(hname.c_str(),htitle.c_str(),1000,ststart,ststop,64*NADC,-0.5,64.*NADC-0.5);
               }
            fmapslice_htsocc[aPulse->sid]->Fill(aPulse->wfts,index);

            runinfo->fRoot->fOutputFile->cd(); // select the correct file
            if( !runinfo->fRoot->fOutputFile->FindObject("slicemap_FEP") )
               runinfo->fRoot->fOutputFile->mkdir("slicemap_FEP");
            runinfo->fRoot->fOutputFile->cd("slicemap_FEP");
            if( !fmapslice_htsFEP.count(aPulse->sid) )
               {
                  std::string hname="htsFEP_slice"; hname+=std::to_string(aPulse->sid);
                  std::string htitle="Pulse per FEP Vs Time  -- Slice # "+std::to_string(aPulse->sid)+";Pulse Time [s];FEP number";
                  fmapslice_htsFEP[aPulse->sid] = new TH2D(hname.c_str(),htitle.c_str(),1000,ststart,ststop,NFEP,0.5,NFEP+0.5);
               }
            fmapslice_htsFEP[aPulse->sid]->Fill(aPulse->wfts,aPulse->fepid);

            runinfo->fRoot->fOutputFile->cd(); // select the correct file
            if( !runinfo->fRoot->fOutputFile->FindObject("slicemap_FEPmap_chan") )
               runinfo->fRoot->fOutputFile->mkdir("slicemap_FEPmap_chan");
            runinfo->fRoot->fOutputFile->cd("slicemap_FEPmap_chan");      
            if( !fmapslice_mapFEP_htsocc[aPulse->sid].count(aPulse->fepid) )
               {
                  std::string hname="htsocc_slice";
                  hname+=std::to_string(aPulse->sid); hname+="_FEP"+std::to_string(aPulse->fepid);
                  std::string htitle="Pulse per Channel Vs Time -- FEP "+std::to_string(aPulse->fepid) +"  -- Slice # "+std::to_string(aPulse->sid)+";Pulse Time [s];Channel Index = number+64*board";
                  //  std::cout<<"Creating:"<<hname<<std::endl;
                  fmapslice_mapFEP_htsocc[aPulse->sid][aPulse->fepid] = new TH2D(hname.c_str(),htitle.c_str(),10000,ststart,ststop,64*NADC,-0.5,64.*NADC-0.5);
               }
            //            std::cout<<"Filling slice: "<<aPulse->sid<<" FEP: "<<aPulse->fepid<<" CH: "<<index<<" with "<<aPulse->wfts<<std::endl;
            fmapslice_mapFEP_htsocc[aPulse->sid][aPulse->fepid]->Fill(aPulse->wfts,index);

            // WAVEFORM HISTOs
            std::vector<uint16_t> theWaveform = aPulse->waveform;
                        
            runinfo->fRoot->fOutputFile->cd(); // select the correct file
            if( !runinfo->fRoot->fOutputFile->FindObject("slicemap_chanph") )
               runinfo->fRoot->fOutputFile->mkdir("slicemap_chanph");
            runinfo->fRoot->fOutputFile->cd("slicemap_chanph");
            if( !fmapslice_hphchan.count(aPulse->sid) )
               {
                  std::string hname="hphchan_slice"; hname+=std::to_string(aPulse->sid);
                  std::string htitle="Pulse Height per Channel -- Slice # "+std::to_string(aPulse->sid)+";Pulse Height [a.u.];Channel Index = number+64*board";
                  fmapslice_hphchan[aPulse->sid] = new TH2D(hname.c_str(),htitle.c_str(),1000,0.,1000.,64*NADC,-0.5,64.*NADC-0.5);
               }
            auto itpulse = std::max_element(theWaveform.begin(),theWaveform.end());
            unsigned int tpulse = std::distance(theWaveform.begin(),itpulse);
            double ph = (double) *itpulse;
            fmapslice_hphchan[aPulse->sid]->Fill(ph,index);

            
            runinfo->fRoot->fOutputFile->cd(); // select the correct file
            if( !runinfo->fRoot->fOutputFile->FindObject("slicemap_chanmap_tsph") )
               runinfo->fRoot->fOutputFile->mkdir("slicemap_chanmap_tsph");
            runinfo->fRoot->fOutputFile->cd("slicemap_chanmap_tsph");      
            if( !fmapslice_mapchan_htsph[aPulse->sid].count(aPulse->GetIndex()) )
               {
                  std::string hname="htsph_slice";
                  hname+=std::to_string(aPulse->sid); hname+="_ch"+std::to_string(aPulse->GetIndex());
                  std::string htitle="Pulse Height Vs Time -- Channel "+std::to_string(aPulse->GetIndex()) +"  -- Slice # "+std::to_string(aPulse->sid)+";Pulse Time [s];Pulse Height [a.u.]";
                  //  std::cout<<"Creating:"<<hname<<std::endl;
                  fmapslice_mapchan_htsph[aPulse->sid][aPulse->GetIndex()] = new TH2D(hname.c_str(),htitle.c_str(),2000,-0.5,1.5,1000,0.,1000.);
                  fmapslice_mapchan_htsph[aPulse->sid][aPulse->GetIndex()]->SetMarkerStyle(8);
                  fmapslice_mapchan_htsph[aPulse->sid][aPulse->GetIndex()]->SetMarkerColor(kBlack);
               }
            double t0 = aPulse->wfts + double(tpulse)*8.e-9; // time of the hit + peak time
            double tt = t0-aPulse->sts; // zero to beginning of slice
            fmapslice_mapchan_htsph[aPulse->sid][aPulse->GetIndex()]->Fill(tt,ph);
         }// pulses loop
   }

   TAFlowEvent* AnalyzeFlowEvent(TARunInfo* runinfo, TAFlags* flags, TAFlowEvent* flow)
   {
      if( !fFlags->fEnabled )
         {
#ifdef HAVE_MANALYZER_PROFILER
            *flags|=TAFlag_SKIP_PROFILE;
#endif
            return flow;
         }
      
      if( fFlags->fVerb )
         std::cout<<"TSPhistoModule:AnalyzeFlowEvent()"<<std::endl;

      SliceEventFlow* sef = flow->Find<SliceEventFlow>();
      if( !sef )
         {
#ifdef HAVE_MANALYZER_PROFILER
            *flags|=TAFlag_SKIP_PROFILE;
#endif
            return flow;
         }
      // else if( fFlags->fVerb )
      //    sef->Print();
      
      std::vector<SlicePulse*>* pulses = sef->fslice_pulse;
      NFEP=sef->fNfep;
      NADC=sef->fNadc; 

      FillHisto(runinfo,pulses,sef->fmidas_ts);
	
      return flow;
   }
};


class TSPhistoFactory: public TAFactory
{
private:
   TSPhistoFlags fFlags;

public:
   void Init(const std::vector<std::string> &args)
   {
      std::cout<<"TSPhistoFactory::Init()"<<std::endl;
      for(size_t i=0; i<args.size(); ++i)
         {
            if( args[i] == "--verbose" )
               fFlags.fVerb=true;
	    else if( args[i] == "--tsphisto" )
               fFlags.fEnabled=true;
         }
   }
   
   void Usage()
   {
      std::cout<<"TSPhisto flags:"<<std::endl;
      std::cout<<"--tsphisto enable this module (histogramming)"<<std::endl;
   }
   
   void Finish()
   {
      std::cout<<"TSPhistoFactory::Finish()"<<std::endl;
   }

   TARunObject* NewRunObject(TARunInfo* runinfo)
   {
      std::cout<<"TSPhistoFactory::NewRunObject()"<<std::endl;
      return new TSPhistoModule(runinfo,&fFlags);
   }
};

static TARegister tar(new TSPhistoFactory);

/* emacs
 * Local Variables:
 * tab-width: 8
 * c-basic-offset: 3
 * indent-tabs-mode: nil
 * End:
 */
