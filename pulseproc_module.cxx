/******************************************************************
 * Raw pulse statistics *
 * 
 * A. Capra
 * March 2022
 *
 ******************************************************************/

#include "manalyzer.h"
#include "midasio.h"
#include "mjson.h"

#include "dsvsflow.h"
#include "dsdata.h"

#include <iostream>
#include <fstream>
#include <cmath>
#include <numeric>
#include <functional>

#include "json.hpp"
using json = nlohmann::json;

#include "baseline.h"


class PulseProcFlags
{
public:
   bool fVerbose;
   std::string fConfigName;
   PulseProcFlags():fVerbose(false)
   {
      std::string basename=BASEDIRECTORY;
      fConfigName=basename+"/config/master.json";
   }
};
  

class PulseProcModule: public TARunObject
{
public:
   PulseProcFlags* fFlags;

private:
   int fCounter; // counts the number of processed events
   int fError;   // counts the number of encountered errors
   
   // Length of filtered waveform baseline
   int nbase_samples; // Pedestal;
   
   // flip wf ?
   bool fInvert;
   int fOffset;

   std::vector<float> fBaselines;

   long int fTotSamples;
   long int fTotPulses;
   unsigned fNChannels;

public:
   PulseProcModule(TARunInfo* runinfo, PulseProcFlags* f): TARunObject(runinfo),
                                                           fFlags(f), fCounter(0), fError(0)
   {
      if(fFlags->fVerbose) std::cout<<"PulseProcModule ctor"<<std::endl;
#ifdef HAVE_MANALYZER_PROFILER
      fModuleName="PulseProc";
#endif

      std::ifstream fin(fFlags->fConfigName.c_str());
      json settings;
      fin>>settings;
      if(fFlags->fVerbose)
         std::cout<<"PulseProcModule Json parsing success!"<<std::endl;
      fin.close();

      nbase_samples=settings["Baseline"]["Pedestal"].get<int>();
      std::cout<<"PulseProcModule ctor Pedestal Length: "<<nbase_samples<<std::endl;
      fInvert=settings["Baseline"]["Invert"].get<bool>();
      std::cout<<"PulseProcModule ctor Flip Signal: "<<fInvert<<std::endl;

      fTotSamples = fTotPulses = fNChannels = 0;
   }


   void BeginRun(TARunInfo* runinfo)
   {
      if(runinfo->fFileName.empty() )
         std::cout<<"PulseProcModule::BeginRun() run: "<<runinfo->fRunNo<<" online"<<std::endl;
      else
         std::cout<<"PulseProcModule::BeginRun() run: "<<runinfo->fRunNo<<" midasfile: "<<runinfo->fFileName.c_str()<<std::endl;

      // runinfo->fOdb->RFA("/VX2740 defaults/DC offset (pct)",&fBaselines);
      // float dynamic_range = pow(2,16)-1;
      // for(auto& b: fBaselines)
      //    {
      //       b *= dynamic_range/100;
      //       std::cout<<b<<", ";
      //    }
      //std::cout<<"\n";
   }

   void EndRun(TARunInfo* runinfo)
   {
      std::cout<<"PulseProcModule::EndRun() run: "<<runinfo->fRunNo<<" events: "<<fCounter<<" errors: "<<fError<<"\tTotal number of Samples: "<<fTotSamples<<"  Total Number of Pulses: "<<fTotPulses<<" Number of Channels: "<<fNChannels<<std::endl;
   }

  
   TAFlowEvent* AnalyzeFlowEvent(TARunInfo* /*runinfo*/, TAFlags* flags, TAFlowEvent* flow)
   {
     SliceEventFlow* sef = flow->Find<SliceEventFlow>();
      if( !sef )
         {
#ifdef HAVE_MANALYZER_PROFILER
            *flags|=TAFlag_SKIP_PROFILE;
#endif
            ++fError;
            return flow;
         }

      std::cout<<"PulseProcModule::AnalyzeFlowEvent # "<<fCounter
               <<"\t # of slices: "<<sef->fNslices<<std::endl;

      std::vector<SlicePulse*>* pulses = sef->fslice_pulse;
      if( !pulses )
         {
#ifdef HAVE_MANALYZER_PROFILER
            *flags|=TAFlag_SKIP_PROFILE;
#endif
            return flow;
         }
      
      if( pulses->size() == 0 ) return flow;
      
      DSProcessorFlow* wf_flow = new DSProcessorFlow(flow, sef->fmidas_sn, sef->fmidas_ts,
                                                     sef->fTSPindex, sef->fSliceIdx,
                                                     sef->fSliceStart);       
      for(auto it=pulses->begin(); it!=pulses->end(); ++it)
         {
            SlicePulse* aPulse = *it;
            ++fTotPulses;
      
            if( fFlags->fVerbose )
               std::cout<<"PulseProcModule:: ";

            // obtain the waveform, we will not modify it
            const std::vector<uint16_t>* adc_samples = aPulse->GetWaveform();
            fTotSamples += adc_samples->size();
            
            // Create channel object 
            TDSChannel* dschan = wf_flow->NewChannel(aPulse->GetIndex(),
                                                     aPulse->bid,aPulse->chid);
            // and carry basic information
            dschan->FEP.push_back(aPulse->fepid);
            dschan->TSP.push_back(aPulse->tspid);
            dschan->PID.push_back(aPulse->pid);
            dschan->SliceIdx.push_back(aPulse->sid);
            dschan->timestamp.push_back(aPulse->wfts);
 
            // calculate baseline and rms 
            uint16_t baseline = get_baseline<uint16_t>(adc_samples,2); // this is the most frequent value of the wf
            if( fBaselines.size() > 0 )
               std::cout<<"ch: "<<dschan->index<<"  baseline: "<<baseline
                        <<" ODB? "<<fBaselines[dschan->index]<<std::endl;
            float baseline_rms = _rms<uint16_t,uint16_t>(adc_samples,baseline);
            // calculate the standard deviation of the whole waveform
            // using the most frequent value of the wf
            float wf_rms= _rms<uint16_t,uint16_t>(adc_samples,baseline,adc_samples->size());

            // Store results
            dschan->baseline.push_back(baseline);
            aPulse->baseline = baseline; // add this to better test FE Processing
            dschan->baseline_rms.push_back(baseline_rms);
            dschan->wf_rms.push_back( wf_rms );

            // std::vector<int32_t> wf = get_subtracted_inverted<int32_t,uint16_t>(adc_samples,baseline,fInvert);
            std::vector<int32_t> wf = get_subtracted_inverted<int32_t,uint16_t>(adc_samples,baseline);
            // Copy unfiltered waveform in the TDSChannel structure
            // after baseline subtraction
            dschan->unfiltered_wf.push_back( wf );

            // get pulse height of waveform and sample number
            auto posit = std::max_element(wf.begin(),wf.end());
            unsigned int pos = std::distance(wf.begin(),posit);
            int32_t ph = *posit;

            // Store results
            dschan->maxpos.push_back(pos);
            dschan->maxph.push_back(ph);
            
            if( fFlags->fVerbose )
               std::cout<<"ch: "<<dschan->index<<"  baseline: "<<baseline
                        <<" RMS: "<<baseline_rms<<" p.height: "<<ph
                        <<" p.min: "<<*std::min_element(wf.begin(),wf.end())
                        <<" length: "<<wf.size()
                        <<" wfrms: "<<wf_rms<<std::endl;
         }// pulse loop

      fNChannels = fNChannels>wf_flow->GetNumberOfChannels()?fNChannels:wf_flow->GetNumberOfChannels();
         
      flow = wf_flow;
      ++fCounter;
      return flow;
   }

};


class PulseProcModuleFactory: public TAFactory
{
public:
   PulseProcFlags fFlags;
   
public:
   void Usage()
   {
      //      printf("Modules options flags:\n");
      printf("\n");
      printf("\t--config, --conf, -c </path/to/json_file>\n\t\tspecify path json configuration file\n");
      printf("\t--verbose, -v\n\t\tprint status information\n");
      printf("\t--skip-events, -s <number>\n\t\tskip to event <number> (starting from 0)\n");
   }

   void Init(const std::vector<std::string> &args)
   {
      printf("PulseProcModuleFactory::Init!\n");
      
      for (unsigned i=0; i<args.size(); i++)
         {
            if( args[i] == "--verbose" ||
                args[i] == "-v" )
               fFlags.fVerbose = true;
            if( args[i] == "--config" ||
                args[i] == "--conf" ||
                args[i] == "-c" )
               fFlags.fConfigName=args[i+1];
         }
   }
   
   void Finish()
   {
      printf("PulseProcModuleFactory::Finish!\n");
   }
   
   TARunObject* NewRunObject(TARunInfo* runinfo)
   {
      printf("PulseProcModuleFactory::NewRunObject, run %d, file %s\n", runinfo->fRunNo, runinfo->fFileName.c_str());
      return new PulseProcModule(runinfo, &fFlags);
   }

};

static TARegister tar(new PulseProcModuleFactory);

/* emacs
 * Local Variables:
 * tab-width: 8
 * c-basic-offset: 3
 * indent-tabs-mode: nil
 * End:
 */
