// *******************************************************
// Analyzer module for TRIUMF DarkSide Vertical Slice
// This module plots pulses
//
// A. Capra - TRIUMF
// July 2021
//
// *******************************************************

#include <iostream>
#include <vector>
#include <string>
#include <map>
#include <array>

#include "manalyzer.h"
#include "dsvsflow.h"

#include "TH1D.h"
#include "TH2D.h"
#include "TDirectory.h"
#include "TCanvas.h"
#include "TLine.h"

class PulsePlotFlags
{
   // command line arguments to modify the behaviour of this module
public:
   bool fVerb=false;
   bool fPulsesPlot = false;
   int fChannelPlot = 0;
   int fSaveCanv = 0;
};

class PulsePlotModule: public TARunObject
{
private:
   std::map<int,std::vector<TCanvas*>> fCanvases; // vector (FEP) of vectors (ADC) of canvases (64 channels)
   unsigned int NFEP=4; // these will change
   unsigned int NADC=8;
   double fph_cut = 180.0;
   int nHistograms=1;

   std::map<double,std::array<unsigned int,3>> fLocation; // location of the waveform in the plots

   std::map<int,TCanvas*> fSingles;
   std::map<double,std::array<unsigned int,4>> fSingLoc; // location of the waveform in the plots
   
public:
   PulsePlotFlags* fFlags = NULL;
   int fCounter;
   int fError;
   const double fMaxADC;

   PulsePlotModule(TARunInfo* runinfo, PulsePlotFlags* f)
      : TARunObject(runinfo),fFlags(f),fCounter(0),fError(0),fMaxADC(65535.)
   {
      std::cout<<"PulsePlotModule::ctor!"<<std::endl;
#ifdef HAVE_MANALYZER_PROFILER
      fModuleName="PulsePlot";
#endif
      
      if( fFlags->fSaveCanv > 0 )
         {
            std::cout<<"Creating canvas ";
            for(size_t i=1; i<=NFEP; ++i) // FEP IDs = 1,2,3,4,...
               {
                  std::vector<TCanvas*> cadc;
                  for(size_t j=0; j<NADC; ++j) // ADC IDs = 0,1,2,...
                     {
                        std::string cname = "cFEP"+std::to_string(i)+"_ADC"+std::to_string(j);
                        std::string ctitle = "FEP "+std::to_string(i)+" ADC "+std::to_string(j);
                        std::cout<<cname<<" ";//<<std::endl;
                        TCanvas* c = new TCanvas(cname.c_str(),ctitle.c_str(),1900,1700);
                        c->Divide(8,8);
                        cadc.push_back(c);
                     }
                  fCanvases[i]=cadc;  
               }
            std::cout<<"\n";
         }
   }
  
   void BeginRun(TARunInfo* /*runinfo*/)
   {
      std::cout<<"PulsePlotModule::BeginRun()"<<std::endl;
      //      runinfo->fRoot->fOutputFile->cd(); // select correct ROOT directory
   }

   void EndRun(TARunInfo* /*runinfo*/)
   {
      std::cout<<"PulsePlotModule::EndRun() Events: "<<fCounter<<" Errors: "<<fError<<std::endl;
   }

   void PlotOneChannel(TARunInfo* runinfo, std::vector<SlicePulse*>* pulses)
   {
#ifdef MODULE_MULTITHREAD
      std::lock_guard<std::mutex> lock(TAMultithreadHelper::gfLock);
#endif
      for(auto it=pulses->begin(); it!=pulses->end(); ++it)
         {
            SlicePulse* aPulse = *it;
            if( fFlags->fChannelPlot>=0 && (int)aPulse->GetIndex() != fFlags->fChannelPlot ) continue;
            std::vector<uint16_t> theWaveform = aPulse->waveform;
            double ts = aPulse->wfts;
            size_t nsamples = theWaveform.size();
            std::string hname = "hFEP"+std::to_string(aPulse->fepid)+"_VX"+std::to_string(aPulse->bid)
               +"_CH"+std::to_string(aPulse->chid)+"_PID"+std::to_string(aPulse->pid);
            //std::cout<<hname<<" "<<nsamples<<std::endl;
            std::string htitle = "TSP"+std::to_string(aPulse->tspid)+" FEP"+std::to_string(aPulse->fepid)
               +" VX"+std::to_string(aPulse->bid)+" CH"+std::to_string(aPulse->chid)+" TS "+std::to_string(ts)+"s;Samples;ADC";

            TDirectory* dir = runinfo->fRoot->fgDir;
            dir->cd();
            //            dir->pwd();
	
            std::string fepdir = "FEP"+std::to_string(aPulse->fepid);
            if( !dir->FindObject(fepdir.c_str()) ) dir->mkdir(fepdir.c_str());
            dir->cd(fepdir.c_str());
            //std::cout<<fepdir<<std::endl;
            //            gDirectory->pwd();

            std::string vxdir = "ADC"+std::to_string(aPulse->bid);
            if( !gDirectory->FindObject(vxdir.c_str()) ) gDirectory->mkdir(vxdir.c_str());
            gDirectory->cd(vxdir.c_str());
            // //std::cout<<vxdir<<std::endl;
            //            gDirectory->pwd();
        
	    // TH1D* hwf = (TH1D*)gDirectory->FindObject(hname.c_str());
	    // if(!hwf)
            //   {
            TH1D* hwf = new TH1D(hname.c_str(), htitle.c_str(), nsamples, 0., double(nsamples));
            hwf->SetLineColor(kBlack);
                  //std::cout<<"PulsePlotModule::ExportAdcs "<<nHistograms++<<" histograms created: "<<hname<<" #s "<<nsamples<<std::endl;
                  //   }
            // std::cout<<"Filling #"<<aPulse->pid<<std::endl;
	    for(size_t isample=0; isample<nsamples; ++isample)
               {
                  // std::cout<<"("<<isample<<","<<theWaveform.at(isample)<<")";
                  //hwf->Fill(isample, double( theWaveform.at(isample) ) );
                  hwf->SetBinContent(isample+1, double( theWaveform.at(isample) ) );
               }

            std::string cname = "cVX"+std::to_string(aPulse->bid)
               +"_CH"+std::to_string(aPulse->chid)+"_PID"+std::to_string(aPulse->pid)+"_SID"+std::to_string(aPulse->sid);
            fSingles[aPulse->pid] = new TCanvas(cname.c_str(),cname.c_str(),1200,1600);
            fSingles[aPulse->pid]->Divide(1,2);
            fSingles[aPulse->pid]->cd(1);
            hwf->Draw("hist");
            fSingLoc[ts]={aPulse->fepid,aPulse->bid,aPulse->chid,aPulse->pid};
         }
   }

   void ExportAdcs(TARunInfo* runinfo, std::vector<SlicePulse*>* pulses)
   {
#ifdef MODULE_MULTITHREAD
      std::lock_guard<std::mutex> lock(TAMultithreadHelper::gfLock);
#endif
      for(auto it=pulses->begin(); it!=pulses->end(); ++it)
         {
            SlicePulse* aPulse = *it;
               
            std::vector<uint16_t> theWaveform = aPulse->waveform;
            double ts = aPulse->wfts;
            size_t nsamples = theWaveform.size();
            std::string hname = "hFEP"+std::to_string(aPulse->fepid)+"_VX"+std::to_string(aPulse->bid)
               +"_CH"+std::to_string(aPulse->chid);
            //std::cout<<hname<<" "<<nsamples<<std::endl;
            std::string htitle = "TSP"+std::to_string(aPulse->tspid)+" FEP"+std::to_string(aPulse->fepid)
               +" VX"+std::to_string(aPulse->bid)+" CH"+std::to_string(aPulse->chid)+";Samples;ADC";

            TDirectory* dir = runinfo->fRoot->fgDir;
            dir->cd();
            //            dir->pwd();
	
            std::string fepdir = "FEP"+std::to_string(aPulse->fepid);
            if( !dir->FindObject(fepdir.c_str()) ) dir->mkdir(fepdir.c_str());
            dir->cd(fepdir.c_str());
            //std::cout<<fepdir<<std::endl;
            //            gDirectory->pwd();

            std::string vxdir = "ADC"+std::to_string(aPulse->bid);
            if( !gDirectory->FindObject(vxdir.c_str()) ) gDirectory->mkdir(vxdir.c_str());
            gDirectory->cd(vxdir.c_str());
            // //std::cout<<vxdir<<std::endl;
            //            gDirectory->pwd();
        
	    TH1D* hwf = (TH1D*)gDirectory->FindObject(hname.c_str());
	    if(!hwf)
               {
                  hwf = new TH1D(hname.c_str(), htitle.c_str(), nsamples, 0., double(nsamples));
                  //  std::cout<<"PulsePlotModule::ExportAdcs "<<nHistograms++<<" histograms created: "<<hname<<" #s "<<nsamples<<std::endl;
               }
            // std::cout<<"Filling #"<<aPulse->pid<<std::endl;
	    for(size_t isample=0; isample<nsamples; ++isample)
               {
                  // std::cout<<"("<<isample<<","<<theWaveform.at(isample)<<")";
                  //hwf->Fill(isample, double( theWaveform.at(isample) ) );
                  hwf->SetBinContent(isample+1, double( theWaveform.at(isample) ) );
               }
            // std::cout<<"\n";

            // std::cout<<"Plotting: "<<fCanvases[aPulse->fepid-1][aPulse->bid]->GetName()<<std::endl;
            if( fFlags->fSaveCanv>0 )
               {
                  
                  if( !fCanvases.count(aPulse->fepid) ) // check the FEP exists
                     {
                        std::vector<TCanvas*> vc;
                        fCanvases[aPulse->fepid]=vc;
                        //   std::cout<<"New FEP "<<aPulse->fepid<<std::endl;
                     }

                  //if( fCanvases[aPulse->fepid].size() < aPulse->bid )
                  //                  it = std::find(arr.begin(), arr.end(), k);
                  int adc_id=aPulse->bid;
                  if( std::find_if(fCanvases[aPulse->fepid].begin(),fCanvases[aPulse->fepid].end(),
                                   [adc_id] (const TCanvas* c) {
                                      if(c) {
                                         std::string adc = "_ADC"+std::to_string(adc_id);
                                         std::string cname(c->GetName());
                                         if( cname.compare(4,adc.size(),adc) ) return false;
                                         else return true;
                                      }
                                      else return false;
                                   } ) != fCanvases[aPulse->fepid].end()  )
                     {
                        std::string cname = "cFEP"+std::to_string(aPulse->fepid)
                           +"_ADC"+std::to_string(aPulse->bid);
                        std::string ctitle = "FEP "+std::to_string(aPulse->fepid)
                           +" ADC "+std::to_string(aPulse->bid);
                        std::cout<<"Creating canvas "<<cname<<std::endl;
                        TCanvas* c = new TCanvas(cname.c_str(),ctitle.c_str(),1900,1700);
                        c->Divide(8,8);
                        fCanvases[aPulse->fepid].push_back( c );
                     }
                  //else
                  // std::cout<<"Plotting: "<<fCanvases[aPulse->fepid][aPulse->bid]->GetName()<<std::endl;

                  fCanvases[aPulse->fepid][aPulse->bid]->cd(aPulse->chid+1);
                  hwf->Draw("hist");
                  fLocation[ts] = {aPulse->fepid,aPulse->bid,aPulse->chid};
               } 
            
         }// loop over pulses
   }

   void AddHits(TARunInfo* runinfo, std::vector<DSVSHit*>* hits)
   {
#ifdef MODULE_MULTITHREAD
      std::lock_guard<std::mutex> lock(TAMultithreadHelper::gfLock);
#endif
      for(auto it=hits->begin(); it!=hits->end(); ++it)
         {
            DSVSHit* aHit = *it;
            if( fFlags->fChannelPlot>=0 && (int)aHit->index != fFlags->fChannelPlot ) continue;
            double ts = aHit->timestamp;
            if( !(fSingLoc.count( ts ) > 0) )
               {
                  std::cerr<<"PulsePlotModule::AddHits TS "<<ts<<" not found"<<std::endl;
                  continue;
               }
            unsigned int fepid = fSingLoc[ts][0]; // fep
            unsigned int board = fSingLoc[ts][1]; // adc
            unsigned int chann = fSingLoc[ts][2]; // chan
            unsigned int uchan = board*64 + chann;
            if( uchan != aHit->index ) continue;
            unsigned int pulid = fSingLoc[ts][3]; // pulse id
            
            std::string hname = "hFEP"+std::to_string(fepid)+"_VX"+std::to_string(board)+"_CH"+std::to_string(chann)+"_PID"+std::to_string(pulid);
            TDirectory* dir = runinfo->fRoot->fgDir;
            dir->cd();
            std::string fepdir = "FEP"+std::to_string(fepid);
            dir->cd(fepdir.c_str());
            std::string vxdir = "ADC"+std::to_string(board);
            gDirectory->cd(vxdir.c_str());

            TH1D* hwf = (TH1D*)gDirectory->FindObject(hname.c_str());
            if( !hwf )
               {
                  std::cerr<<"PulsePlotModule::AddHits Histo "<<hname<<" not found"<<std::endl;
                  continue;
               }
            
            // std::cout<<"PulsePlotModule::AddHits"<<hwf->GetName()<<"\tCH: "<<uchan<<"\tts: "<<ts;
            double x = aHit->time;
            double y1 = hwf->GetBinContent( hwf->GetMinimumBin() );
            double y2 = hwf->GetBinContent( hwf->GetMaximumBin() );
            // std::cout<<"\tidx: "<<aHit->index<<"  t: "<<x<<"  h: "<<aHit->height<<std::endl;
            
            TLine* l = new TLine(x,y1,x,y2);
            l->SetLineColor(kRed);
            if( !fSingles[pulid] )
               {
                  std::cerr<<"PulsePlotModule::AddHits Canvas @ "<<pulid<<" not found"<<std::endl;
                  continue;
               }
            fSingles[pulid]->cd(1);
            l->Draw("same");
         }
   }

   void AddChannels(TARunInfo* runinfo,std::vector<TDSChannel*>* chans)
   {
#ifdef MODULE_MULTITHREAD
      std::lock_guard<std::mutex> lock(TAMultithreadHelper::gfLock);
#endif
      for(auto it=chans->begin(); it!=chans->end(); ++it)
         {
            TDSChannel* aCh = *it;
            unsigned bid = aCh->module;
            unsigned chid = aCh->index;
            if( fFlags->fChannelPlot>=0 && (int) chid != fFlags->fChannelPlot ) continue;
            aCh->Print();
            for(size_t j=0; j<aCh->timestamp.size(); ++j)
               {
                  double ts = aCh->timestamp[j];
                  //                  std::cout<<ts<<std::endl;
                  unsigned pid = aCh->PID[j];
                  unsigned fepid = aCh->FEP[j];
                  unsigned tspid = aCh->TSP[j];
             
                  std::vector<int32_t> aHit = aCh->MAsub_wf[j];               

                  size_t nsamples = aHit.size();
                  //std::cout<<nsamples<<std::endl;
                  std::string hname = "hhit_CH"+std::to_string(chid)+"_PID"+std::to_string(pid);
                  //                  std::cout<<hname<<" "<<nsamples<<std::endl;
                  std::string htitle = "TSP"+std::to_string(tspid)+" CH"+std::to_string(chid)+" TS "+std::to_string(ts)+"s;Samples;ADC";

                  TDirectory* dir = runinfo->fRoot->fgDir;
                  dir->cd();
                  //            dir->pwd();
	
                  std::string fepdir = "FEP"+std::to_string(fepid);
                  if( !dir->FindObject(fepdir.c_str()) ) dir->mkdir(fepdir.c_str());
                  dir->cd(fepdir.c_str());
                  //std::cout<<fepdir<<std::endl;
                  //            gDirectory->pwd();

                  std::string vxdir = "ADC"+std::to_string(bid);
                  if( !gDirectory->FindObject(vxdir.c_str()) ) gDirectory->mkdir(vxdir.c_str());
                  gDirectory->cd(vxdir.c_str());
                  // //std::cout<<vxdir<<std::endl;
                  //            gDirectory->pwd();

                  TH1D* hhit = new TH1D(hname.c_str(), htitle.c_str(), nsamples, 0., double(nsamples));
                  hhit->SetMinimum(0);
                  hhit->SetLineColor(kBlue);

                  for(size_t isample=0; isample<nsamples; ++isample)
                     {
                        hhit->SetBinContent(isample+1, double( aHit.at(isample) ) );
                     }
                  
                  fSingles[pid]->cd(2);
                  hhit->Draw("hist");
               }
         }
   }

   
   TAFlowEvent* AnalyzeFlowEvent(TARunInfo* runinfo, TAFlags* flags, TAFlowEvent* flow)
   {
      if( !fFlags->fPulsesPlot )
         {
#ifdef HAVE_MANALYZER_PROFILER
            *flags|=TAFlag_SKIP_PROFILE;
#endif
            return flow;
         }

      SliceEventFlow* sef = flow->Find<SliceEventFlow>();
      if( !sef )
         {
#ifdef HAVE_MANALYZER_PROFILER
            *flags|=TAFlag_SKIP_PROFILE;
#endif
            ++fError;
            return flow;
         }

      std::cout<<"PulsePlotModule::AnalyzeFlowEvent # "<<fCounter<<"\t # of slices: "<<sef->fNslices<<std::endl;
      if( fFlags->fVerb )
         {
            sef->Print();
            std::cout<<"---------------------------------"<<std::endl;
         }

      std::vector<SlicePulse*>* pulses = sef->fslice_pulse;
      if( !pulses )
         {
#ifdef HAVE_MANALYZER_PROFILER
            *flags|=TAFlag_SKIP_PROFILE;
#endif
            return flow;
         }
      
      if( pulses->size() > 0 )
         {
            std::cout<<"PulsePlotModule::AnalyzeFlowEvent Plotting Event "<<fCounter
                     <<" Number of Pulses: "<<pulses->size()<<std::endl;
            //ExportAdcs(runinfo, pulses);
            PlotOneChannel(runinfo, pulses);
            //            std::cout<<"PulsePlotModule::AnalyzeFlowEvent ADCs exported!"<<std::endl;
         }

      DSProcessorFlow* wf_flow = flow->Find<DSProcessorFlow>();
      if( !wf_flow ) 
         {
            ++fError;
#ifdef HAVE_MANALYZER_PROFILER
            *flags|=TAFlag_SKIP_PROFILE;
#endif
            return flow;
         }

      std::vector<DSVSHit*>* hits = wf_flow->hits;
      if( hits->size() > 0 )
         {
            std::cout<<"PulsePlotModule::AnalyzeFlowEvent Adding hits in Event: "<<fCounter
                     <<" Number of Hits: "<<hits->size()<<std::endl;
            AddHits(runinfo, hits);
         }
      
      std::vector<TDSChannel*>* chans = wf_flow->channels;
      if( chans->size() > 0 )
         {
            std::cout<<"PulsePlotModule::AnalyzeFlowEvent Filtered Waveforms in Event: "<<fCounter
                     <<" Number of Channels: "<<chans->size()<<std::endl;
            AddChannels(runinfo,chans);
         }
   
      if( fCounter < fFlags->fSaveCanv ) // save canvases for later inspection
         {
            // for(size_t i=1; i<=NFEP; ++i)
            //    {
            //       for(size_t j=0; j<NADC; ++j)
            //          {
            //             std::string sname(fCanvases.at(i).at(j)->GetName());
            //             sname+="_Event"+std::to_string(fCounter)
            //                +"_TSP"+std::to_string(sef->fTSPindex)+".png";
            //             fCanvases.at(i).at(j)->SaveAs(sname.c_str());
            //          }
            //    }
            int np=0;
            for( auto it=fSingles.begin(); it!=fSingles.end(); ++it)
               {
                  std::cout<<"PulsePlotModule::AnalyzeFlowEvent Saving "<<++np<<std::endl;
                  it->second->SaveAs(".png");
               }
         }
      
      ++fCounter;
      return flow;
   }
  
};

class PulsePlotFactory: public TAFactory
{
public:
   PulsePlotFlags fFlags;
   
public:
   void Usage()
   {
      std::cout<<"PulsePlot flags:"<<std::endl;
      std::cout<<"--verbose show what's happening"<<std::endl;
      std::cout<<"--pulses plot waveform from all pulses to JSROOT"<<std::endl;
      std::cout<<"--pulses <channel> plot waveform channel to JSROOT"<<std::endl;
      std::cout<<"--print <number> save to png <number> of events"<<std::endl;
   }

   void Init(const std::vector<std::string> &args)
   {
      std::cout<<"PulsePlotFactory::Init!"<<std::endl;

      for( unsigned i=0; i<args.size(); i++)
         {
            if( args[i] == "--verbose" )
               fFlags.fVerb=true;
            if( args[i] == "--pulses" )  // plot all pulses in TH1D
               fFlags.fPulsesPlot = true;
            if( args[i] == "--plot" ){  // plot given channel to TH1D
               fFlags.fChannelPlot = std::stoi(args[i+1]);
               fFlags.fPulsesPlot = true;
            }
            if( args[i] == "--print" )  // save canvases as png
               fFlags.fSaveCanv = std::stoi(args[i+1]);
         }
   }

   void Finish()
   {
      std::cout<<"PulsePlotFactory::Finish!"<<std::endl;
   }

   TARunObject* NewRunObject(TARunInfo* runinfo)
   {
      std::cout<<"PulsePlotFactory::NewRunObject"<<std::endl;
      return new PulsePlotModule(runinfo, &fFlags);
   }
};

static TARegister tar(new PulsePlotFactory);

/* emacs
 * Local Variables:
 * tab-width: 8
 * c-basic-offset: 3
 * indent-tabs-mode: nil
 * End:
 */
