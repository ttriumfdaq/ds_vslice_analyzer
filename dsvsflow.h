// *******************************************************
// Analyzer module for TRIUMF DarkSide Vertical Slice
// Flow object to pass data slice object data to modules
//
// A. Capra - TRIUMF
// July 2021
//
// *******************************************************


#ifndef __DSVSFLOW__
#define __DSVSFLOW__

#include "manalyzer.h"

#include <cstdint>
#include <iostream>
#include <vector>

#include "dsdata.h"


class SliceEventFlow: public TAFlowEvent
{
public:
  unsigned int fNadc;    // number of boards read out
  unsigned int fNfep;    // number of FEPs read out
  unsigned int fNslices; // number of slices = number of TSP banks = number of slice->decode calls
  double fmidas_ts;      // midas timestamp in seconds (UNIX)
  int fmidas_sn;         // midas event serial number
  unsigned int fTSPindex;// TSP node id
  unsigned int fSliceIdx;// Slice index
  double fSliceStart;    // Slice TimeStamp in sec. since BOR
  
  std::vector<SlicePulse*>* fslice_pulse; // vector of custom objects that contains wf with channel id
  std::vector<DSVSHit*>* fhits; // vector of custom objects that contains wf with channel id

public:
  SliceEventFlow(TAFlowEvent* flow):TAFlowEvent(flow),
				    fNadc(0),fNfep(0),fNslices(0),
                                    fmidas_ts(0),fmidas_sn(0),
				    fTSPindex(0),fSliceIdx(0),
				    fSliceStart(0.),fslice_pulse(0)
  {  }

  SliceEventFlow(TAFlowEvent* flow,
		 unsigned int a, unsigned int f, unsigned int s,
		 double t, int e):TAFlowEvent(flow),
				  fNadc(a),fNfep(f),fNslices(s),
				  fmidas_ts(t),fmidas_sn(e),
				  fTSPindex(0),fSliceIdx(0),
				  fSliceStart(0.),fslice_pulse(0)
  {  }
  
  ~SliceEventFlow()
  {
    if(fslice_pulse)   delete fslice_pulse;
  }

  void SetPulses(std::vector<SlicePulse*>& p)
  {
    fslice_pulse = new std::vector<SlicePulse*>(p);
  }

  void Print()
  {
    if( fNslices > 0 )
      std::cout<<"SliceEventFlow # Slices: "<<fNslices
	       <<" MIDAS TS: "<<fmidas_ts<<"s  MIDAS S/N: "<<fmidas_sn
	       <<"  number of ADCs: "<<fNadc
	       <<"  number of FEPs: "<<fNfep
	       <<"   TSP index: "<<fTSPindex
	       <<"  Slice TS: "<<fSliceStart
	       <<"s  Slice index: "<<fSliceIdx
	       <<"   number of pulses: "<<fslice_pulse->size()<<std::endl;
    else
      std::cerr<<"SliceEventFlow::Print() Error"<<std::endl;
  }
};



class DSProcessorFlow: public TAFlowEvent
{
public:
  std::vector<DSVSHit*>* hits;
  std::vector<TDSChannel*>* channels;
  int fEventNumber;
  double fEventTS;       // midas timestamp in seconds (UNIX)
  unsigned int fTSPindex;// TSP node id
  unsigned int fSliceIdx;// Slice index
  double fSliceStart;    // Slice TimeStamp in sec. since BOR

public:
  DSProcessorFlow(TAFlowEvent* flow):TAFlowEvent(flow)
  {
    hits = new std::vector<DSVSHit*>();
    channels = new std::vector<TDSChannel*>();
  }

  DSProcessorFlow(TAFlowEvent* flow, int event, double midas_ts,
		  unsigned int tsp_index,unsigned int slice_index,
		  double slice_start):TAFlowEvent(flow),fEventNumber(event),fEventTS(midas_ts),
				      fTSPindex(tsp_index),fSliceIdx(slice_index),
				      fSliceStart(slice_start)
  {
    hits = new std::vector<DSVSHit*>;
    channels = new std::vector<TDSChannel*>;
  }

  TDSChannel* NewChannel(const unsigned int& idx,
			 const int& adc, const int& ch)
  {
    for(auto c : *channels)
      {
	if( c->index == idx ) return c;
      }
    
    TDSChannel* newchan = new TDSChannel(adc, ch);
    channels->push_back(newchan);
    return newchan;
  }

  ~DSProcessorFlow()
  {
    if( hits )
      {
	for(auto& h : *hits)
	  if(h) delete h;
	hits->clear();
	delete hits;
      }

    if( channels )
      {
	for(auto& c : *channels)
	  if(c) delete c;
	channels->clear();
	delete channels;
      }
  }

  inline unsigned int GetNumberOfChannels() const { return channels->size(); }
  inline TDSChannel* GetDSchan(int i)       const { return channels->at(i); }
  inline unsigned int GetNumberOfHits()     const { return hits->size(); }
  inline const DSVSHit* GetHit(unsigned i)  const { return hits->at(i); }

};

#endif

/* emacs
 * Local Variables:
 * tab-width: 8
 * c-basic-offset: 3
 * indent-tabs-mode: nil
 * End:
 */
