/******************************************************************
 *  Hit Finder algorithms *
 * 
 * A. Capra
 * April 2022
 *
 ******************************************************************/


class HitFinder
{
public:
  HitFinder():fWindow(100),fThreshold(10),fMinIntegral(100)
  {
    fHalfWindow = fWindow/2;
    Reset();
  }
  
  HitFinder(uint32_t w, uint16_t thr, int32_t mq):fWindow(w),fThreshold(thr),fMinIntegral(mq)
  {
    fHalfWindow = fWindow/2;
    Reset();
  }

  void moving_average_subtraction(const std::vector<int32_t>* adc_samples)
  {
    if( adc_samples->size() < fWindow ) return;
    fOut.resize( adc_samples->size() );

    // partial sum
    int32_t asum = 0;
    // indexes
    size_t i=0,j=1;
    
    // first point
    for( ; i<fHalfWindow; ++i) asum += 2*adc_samples->at(i);
    fOut[0] = adc_samples->at(0) - asum/fWindow;

    // first half window
    for( i=1; i<=fHalfWindow; ++i )
      {
	asum += adc_samples->at( i + fHalfWindow - 1 ) - adc_samples->at( fHalfWindow - i );
	fOut[i] = adc_samples->at(i) - asum/fWindow;
      }

    // first from half window to number of samples minus half window
    for( i=fHalfWindow+1; i<=adc_samples->size()-fHalfWindow; ++i )
      {
	asum += adc_samples->at( i + fHalfWindow - 1 ) - adc_samples->at( i - fHalfWindow - 1 );
	fOut[i] = adc_samples->at(i) - asum/fWindow;
      }

    // from number of samples minus half window to number of samples
    for( i=adc_samples->size()-fHalfWindow+1; i<adc_samples->size(); ++i )
      {
	asum += adc_samples->at( adc_samples->size() - j++ ) - adc_samples->at( i - fHalfWindow - 1 );
	fOut[i] = adc_samples->at(i) - asum/fWindow;
      }
  }

  void peak_finder(const std::vector<int32_t>* adc_samples)
  {
    auto it = fOut.begin();
    int32_t thr=fThreshold;
    while(true)
      {
	// find where waveform is above thr
	it = std::find_if( it, fOut.end(),
			   [thr](int32_t const& r)
			   { return (r>thr); } );
	// if it's never above thr, quit
	if( it==fOut.end() ) break;
	
	uint32_t iStart = std::distance(fOut.begin(),it);
	// std::cout<<"\t"<<iStart;

	// find where waveform goes back below thr
	it = std::find_if( it, fOut.end(),
			   [thr](int32_t const& r)
			   { return (r<thr); } );
	uint32_t iEnd = std::distance(fOut.begin(),it);
	// std::cout<<" "<<iEnd;

        // find peak pulse
	std::vector<int32_t>::iterator max_pos = std::max_element(fOut.begin()+iStart,
                                                                  fOut.begin()+iEnd);
	//	std::cout<<" "<<smax;
	int32_t A = *max_pos;
        // std::cout<<" "<<A<<" = "<<static_cast<uint16_t>(A);
	
	// if the pulse is too 'small'
	int32_t Q = std::accumulate(fOut.begin()+iStart,fOut.begin()+iEnd,int32_t(0));
	//std::cout<<" "<<Q<<"\t"<<Q/A<<std::endl;
	if( Q/A < fMinIntegral )
	  {
	    ++it;
	    //  std::cout<<" too short"<<std::endl;
	    continue;
	  }

        // peak-width
	uint32_t duration = iEnd-iStart;
	
        // peak time (sample)
	uint16_t smax = std::distance(fOut.begin(),max_pos);

        uint16_t prom = static_cast<uint16_t>(adc_samples->at(smax));
        
        fHits.emplace_back(prom,smax,duration,Q);
        //fHits.emplace_back(static_cast<uint16_t>(A),smax,duration,Q);
	++it;
      }
  }

  void operator()(const std::vector<int32_t>* adc_samples)
  {
    moving_average_subtraction( adc_samples );
    peak_finder( adc_samples );
   }

  // setters
  inline void SetMAWindow(unsigned w)    { fWindow=w; fHalfWindow=fWindow/2; }
  inline void SetThreshold(unsigned t)   { fThreshold=t; }
  inline void SetMinIntegral(int q)      { fMinIntegral=q; }

  // getters
  inline const std::vector<int32_t>* GetWF() const { return &fOut; }

  struct SingleHit
  {
    uint16_t k;
    uint16_t t;
    uint16_t Dt;
    int32_t Q;

  SingleHit(uint16_t a, uint32_t b, uint32_t c):k(a),t(b),Dt(c),Q(0)
    {}
  SingleHit(uint16_t a, uint32_t b, uint32_t c,int32_t d):k(a),t(b),Dt(c),Q(d)
    {}
  };

  // getters
  inline uint16_t Getk(size_t i)  const { return fHits[i].k; }
  inline uint16_t Gett(size_t i)  const { return fHits[i].t; }
  inline uint16_t GetDt(size_t i) const { return fHits[i].Dt; }
  inline int32_t GetQ(size_t i)   const { return fHits[i].Q; }
  inline size_t GetNumberOfHits() const { return fHits.size(); }

  void Reset()
  {
     fOut.clear();
     fHits.clear();
  }

  void Print()
  {
    for(auto& h: fHits)
      std::cout<<"k: "<<h.k<<" t: "<<h.t<<" Dt: "<<h.Dt<<" Q: "<<h.Q<<"\tcut: "<<h.Q/h.k<<std::endl;
  }

private:
  uint32_t fWindow;
  uint32_t fHalfWindow;
  int32_t fThreshold;
  int32_t fMinIntegral;
  std::vector<int32_t> fOut;
  std::vector<SingleHit> fHits;
};


/* emacs
 * Local Variables:
 * tab-width: 8
 * c-basic-offset: 3
 * indent-tabs-mode: nil
 * End:
 */
