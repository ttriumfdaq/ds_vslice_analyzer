#ifndef __DSDATA__
#define __DSDATA__

#include <iostream>
#include <iomanip>
#include <vector>

// WFD: VX2740
//   - Number of Channels Per Module: 64;
//   - Sampling Period: 8 ns ; // 125 MS/s = 8 ns
//   - Resolution: 0xffff; // 16 bits

/// Class to store a pulse
class DSVSHit
{
public:
  DSVSHit(int imodule = -1, int ichannel = -1):time(0),height(0),/*charge(0.),*/
      /*tot(0),*/timestamp(0.)
  {
    index = static_cast<uint16_t>(imodule*64 + ichannel);
  }

  DSVSHit(uint16_t ii):index(ii),time(0),height(0),/*charge(0.),*/
      /*tot(0),*/timestamp(0.)
  {  }

  DSVSHit(uint16_t ii, double ts):index(ii),time(0),height(0),timestamp(ts)
  {  }
  
  uint16_t index;
  uint16_t time;
  uint16_t height;
  //  double charge;
  //  uint32_t tot;
  double timestamp;
   
  void Print() const
   {
      std::cout<<"DSVSHit ch:"<<std::setw(5)<<index
               <<" H:"<<std::setw(6)<<height
         //               <<"\t Q: "<<charge
	       <<" t:"<<std::setw(6)<<time
         //               <<" Dt:"<<std::setw(6)<<tot
	       <<" ts: "<<timestamp<<std::endl;
   }
};


/// Class to store info about a waveform (ie baseline)
class TDSChannel
{
public:
  // ctor
  TDSChannel(): module(-1),channel(-1),index(99999999)
  { }
  
  TDSChannel(const int imodule, const int ichannel):
             module(imodule),channel(ichannel),index(imodule*64 + ichannel)
  { }

  // dtor
  virtual ~TDSChannel()
  {
    for( auto& w : unfiltered_wf )
      w.clear();
    unfiltered_wf.clear();

    for( auto& w : filtered_wf )
      w.clear();
    filtered_wf.clear();

    for( auto& w : MAsub_wf )
       w.clear();
    MAsub_wf.clear();

    FEP.clear();
    TSP.clear();
    timestamp.clear();

    baseline.clear();
    baseline_rms.clear();
    maxph.clear();
    maxpos.clear();
    wf_rms.clear();
  }
  
  // raw pulse info
  std::vector<uint16_t> baseline;
  std::vector<float> baseline_rms;
  std::vector<int32_t> maxph;
  std::vector<unsigned int> maxpos;
  std::vector<float> wf_rms;

  // filtered pulse info
  std::vector<int32_t> filter_baseline;
  std::vector<float> filter_rms;
  std::vector<int32_t> filter_maxph;
  std::vector<unsigned int> filter_maxpos;
  std::vector<float> filterwf_rms;

  // raw pulse
  std::vector<std::vector<int32_t>> unfiltered_wf;
  // filtered pulse
  std::vector<std::vector<int32_t>> filtered_wf;
  // MA-subtracted pulse
  std::vector<std::vector<int32_t>> MAsub_wf;
  
public: // for now
  const int module; // sequential ADC number
  const int channel;// ADC channel number
  const unsigned int index;// sequential channel number
  std::vector<unsigned int> FEP;
  std::vector<unsigned int> TSP;
  std::vector<unsigned int> SliceIdx;
  std::vector<double> timestamp;
  std::vector<unsigned int> PID;

public:
  
  void Print() const
   {
      std::cout<<"TDSChannel\tVX: "<<module<<"\tCH: "<<channel<<"\tIDX: "<<index
	//<<"\t b: "<<baseline<<" rms: "<<baseline_rms<<"\tAmplitude: "<<maxph
	       <<"\tsize: "<<timestamp.size()<<std::endl;
   }
};

class SlicePulse
{
public:
  const unsigned tspid; // TSP index 1..5?
  const unsigned fepid; // FEP index 1..4
  const unsigned bid;   // VX index 0..3
  const unsigned chid;  // channel index 0..63
  const unsigned pid;   // it can be large...
  std::vector<uint16_t> waveform;
  uint16_t baseline; // for FEP processing (test)

  const unsigned sid; // slice index
  const double sts;   // slice timestamp [seconds since start of run]

  const double wfts; // waveform timestamp [seconds since start of run]
  
  SlicePulse(unsigned t, unsigned f, unsigned b, unsigned c, unsigned p,
	     std::vector<uint16_t>* wf, unsigned i, double ts, double tt):
             tspid(t), fepid(f), bid(b), chid(c),
             pid(p), waveform(*wf), baseline(0),
             sid(i), sts(ts), wfts(tt)
  { }

  ~SlicePulse()
  {
    waveform.clear();
  }

  // calculate sequential indexing for channels
  inline unsigned GetIndex() const
  {
    //    std::cout<<chid<<" + 64x"<<bid<<" = "<<chid+64*bid<<std::endl;
    const unsigned the_index = chid+64*bid;
    return the_index;
  }

  inline const std::vector<uint16_t>* GetWaveform() const
  {
     return &waveform;
  }

  void print()
   {
      std::cout<<"SlicePulse TSP: "<<tspid<<" FEP: "<<fepid<<" ADC: "<<bid<<" CH: "<<chid
	       <<" pulse # "<<pid<<" wf size: "<<waveform.size()
	       <<" slice index: "<<sid<<" ts: "<<sts<<"s pulse ts: "<<wfts<<"s"<<std::endl;
   }
};

#endif


/* emacs
 * Local Variables:
 * tab-width: 8
 * c-basic-offset: 3
 * indent-tabs-mode: nil
 * End:
 */
