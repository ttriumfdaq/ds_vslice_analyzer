/******************************************************************
 *  Filtering algorithms *
 * 
 * A. Capra
 * January 2022
 *
 ******************************************************************/


#include <vector>
#include <numeric>
#include <fstream>
#include <string>
#include <cassert>
#include <cmath>

class Filter
{
 public:
   Filter()
      {}

   virtual ~Filter()
      {
         hit.clear();
      }

   virtual std::vector<uint16_t>* operator()(const std::vector<uint16_t>* adc_samples)
   {
      Reset(0);
      std::for_each(adc_samples->begin(),adc_samples->end(),
                    [this](const uint16_t& x) { hit.push_back(x);} );
      return &hit;
   }

 public:
   std::vector<uint16_t> hit;

   virtual void Reset(size_t nsamples)
   {
      hit.resize(nsamples);
   }
};

class MAF: public Filter
{
 public:
 MAF():Filter(),w(1){} // null filter

 MAF(int k):Filter(),w(k)
   {
      assert(w>0);
   }

   std::vector<uint16_t>* operator()(const std::vector<uint16_t>* adc_samples)
      {
         int n=adc_samples->size();
         Reset(n);
         int sum=0;
         for(int i=0; i<=w; ++i)
            {
               sum+=adc_samples->at(i);
               hit[i]=static_cast<uint16_t>(sum/(i+1));
            }
         for( int i=w+1;i<n;++i )
            {
               hit[i]=static_cast<uint16_t>( hit[i-1]+(adc_samples->at(i)-adc_samples->at(i-w))/w );
            }
         return &hit;
      }

   int GetGate() const { return w; }

 private:
   int w;
};

class EXF: public Filter
{
 public:
 EXF():Filter(),w(1),w1(0){} //null filter
   
 EXF(double a):Filter()
      {
         assert((a>0.) && (a<1.));
         w=a;
         w1=1.-w;
      }

   virtual std::vector<uint16_t>* operator()(const std::vector<uint16_t>* adc_samples)
   {
      int n=adc_samples->size();
      Reset(n);
      hit[0]=adc_samples->at(0);
      for(int i=1;i<n;++i)
         {
            hit[i]=static_cast<uint16_t>( w*adc_samples->at(i)+w1*hit[i-1] );
         }
      return &hit;
   }


 private:
   double w;
   double w1;
};

class ARF
{
 public:
 ARF():ftau(0.)
      {
         FixParameters();
      }
   
 ARF(double t):ftau(t)
   {
      FixParameters();
   }

   std::vector<int32_t>* operator()(const std::vector<int32_t>* adc_samples)
      {
         Reset(adc_samples->size());
    
         // prepare AR kernel
         size_t nsamples=adc_samples->size();
         AR.resize(nsamples);
         AR[0]=static_cast<double>(adc_samples->back());
      
         size_t i;
         // Auto-Recursive filtering for the exponential tail
         for( i=1; i<nsamples; ++i)
            {
               AR[i] = static_cast<double>(adc_samples->at(nsamples-1-i)) + _a*AR[i - 1];
            }

         // create output
         for(i = 0; i<nsamples; ++i)
            {
               hit[i] = static_cast<int32_t>( AR[nsamples-1-i] * _tau );
            }
         return &hit;
      }

 private:
   void FixParameters()
   {
      _tau =  1./ftau;
      _a = 1. - _tau;
   }

   inline void Reset(size_t nsamples)
   {
      hit.resize(nsamples);
   }

  
 private:
   // filter parameters
   double ftau;

   // convenience variables
   double _a;
   double _tau;

   // Auto-Recursive vector
   std::vector<double> AR;

   // output
   std::vector<int32_t> hit;
};

class ARMA
{
 public:
 ARMA():ftau(0.),fsigma(0.),ffp_exp_ratio(0.)
      {
         kgsize=0;
         FixParameters();
      }
   
 ARMA(double t, double s, double k):ftau(t),fsigma(s),ffp_exp_ratio(k)
   {
      gen_gaus_kernel();
      if( !(kgsize > 0) ) std::cerr<<"ARMA ctor: something wrong with the Gaussian kernel"<<std::endl;
      FixParameters();
   }

   std::vector<int32_t>* operator()(const std::vector<int32_t>* adc_samples)
      {
         // prepare filter kernels
         size_t nsamples= adc_samples->size();
         Reset(nsamples);
         MA.resize(nsamples);
         AR.resize(nsamples);
         AR[0]=static_cast<double>(adc_samples->back());
      
         size_t i, j;
         double tmp;
         for( i=0; i<nsamples; ++i)
            {
               if( (int(i - ceiled) >= 0) && (i + halfsize < nsamples) )  // take care of waveform edges
                  {
                     tmp = 0.; // zero partial summation 
                     for(j=0; j<kgsize; ++j) // cross-correlation filtering for the fast peak
                        {
                           tmp += static_cast<double>(adc_samples->at( i + halfsize - j )) * gker[j];
                        }
                     MA[i] = tmp; 
                  }
               else
                  MA[i] = 0.;

               // Auto-Recursive filtering for the exponential tail
               if( i ) AR[i] = static_cast<double>(adc_samples->at(nsamples-1-i)) + _a*AR[i - 1];
            }

         // ARMA: put together
         for(i = 0; i<nsamples; ++i)
            {
               hit[i] = static_cast<int32_t>( _scale_factor * AR[nsamples-1-i] * _tau  + scale_factor * MA[i] );
            }
         return &hit;
      }

   inline void SetScaleFactor(double f) { scale_factor = f; _scale_factor = 1. - scale_factor; }


 private:
   void gen_gaus_kernel()
   {
      gker.clear();
      kgsize = static_cast<int>(8*fsigma);
      for(size_t i=0; i<kgsize; ++i)
         gker.push_back(exp( -0.5*pow( (i-4.*fsigma)/fsigma,2) ) / (fsigma*sqrt(2*M_PI)) );
   }
  
   void FixParameters()
   {
      ceiled = ceil(kgsize / 2 ) + 1;
      halfsize = kgsize / 2;
    
      scale_factor = ffp_exp_ratio/ftau*(sqrt(2*M_PI)*fsigma);
      _scale_factor = 1. - scale_factor;
      _tau =  1./ftau;
      _a = 1. - _tau;
   }

   inline void Reset(size_t nsamples)
   {
      if( nsamples <= kgsize )
         std::cerr<<"ARMA Reset: Number of samples: "<<nsamples
                  <<" smaller than the Gaussian kernel size: "<<kgsize<<std::endl;
      hit.resize(nsamples);
   }


 private:
   // filter parameters
   double ftau; // in samples
   double fsigma; // in samples
   double ffp_exp_ratio;

   // convenience variables
   double _a;
   double _tau; // 1/tau : 1/samples

   // combine (1-scale_factor)*AR+scale_factor*MA
   double scale_factor;
   double _scale_factor;

   // gaussian kernel vector
   std::vector<double> gker;
   size_t kgsize;
   size_t ceiled;
   size_t halfsize;

   // finite response vector
   std::vector<double> MA;
   // Auto-Recursive vector
   std::vector<double> AR;

   // output
   std::vector<int32_t> hit;
};

class ARFb: public Filter
{
 public:
 ARFb():Filter(),ftau(0.),flow(5),fup(10)
      {
         FixParameters();
      }
   
 ARFb(double t, size_t l=5, size_t u=10):Filter(),ftau(t),flow(l),fup(u)
   {
      FixParameters();
   }

   std::vector<uint16_t>* operator()(const std::vector<uint16_t>* adc_samples)
      {
         Reset(adc_samples->size());
    
         // prepare wf to be boardered
         std::vector<uint16_t> wf(adc_samples->begin(),adc_samples->end());

         // boarded matrix to treat wf edges
         wf.insert(wf.begin(),adc_samples->end()-low_board,adc_samples->end());
         wf.insert(wf.end(),adc_samples->end()-up_board,adc_samples->end());

         // prepare AR kernel
         size_t nsamples=wf.size();
         AR.resize(nsamples);
         AR[0]=static_cast<double>(wf.back());
      
         size_t i;
         // Auto-Recursive filtering for the exponential tail
         for( i=1; i<nsamples; ++i)
            {
               AR[i] = static_cast<double>(wf[nsamples-1-i]) + _a*AR[i - 1];
            }
    
         size_t k;
         // create output
         for(i = low_board; i<nsamples-up_board; ++i)
            {
               k = i-low_board;
               hit[k]=static_cast<uint16_t>( AR[nsamples-1-i] * _tau );
            }
         return &hit;
      }

 private:
   void FixParameters()
   {
      _tau =  1./ftau;
      _a = 1. - _tau;
      fboard = static_cast<int>(ftau);
      low_board = flow*fboard;
      up_board = fup*fboard;
   }

  
 private:
   // filter parameters
   double ftau;

   // convenience variables
   double _a;
   double _tau;
   int fboard;
   size_t flow;
   size_t fup;
   // number of samples for boarding 
   size_t low_board; // samples
   size_t  up_board; // samples

   // Auto-Recursive vector
   std::vector<double> AR;
};

class ARMAb: public Filter
{
 public:
 ARMAb():Filter(),ftau(0.),fsigma(0.),ffp_exp_ratio(0.),flow(5),fup(10)
      {
         kgsize=0;
         FixParameters();
      }
   
 ARMAb(double t, double s, double k, size_t l=5, size_t u=10):Filter(),
      ftau(t),fsigma(s),ffp_exp_ratio(k),
      flow(l),fup(u)
   {
      gen_gaus_kernel();
      if( !(kgsize > 0) ) std::cerr<<"ARMA ctor: something wrong with the Gaussian kernel"<<std::endl;
      FixParameters();
   }

   std::vector<uint16_t>* operator()(const std::vector<uint16_t>* adc_samples)
      {
         Reset(adc_samples->size());
    
         // prepare wf to be boardered
         std::vector<uint16_t> wf(adc_samples->begin(),adc_samples->end());

         // boarded matrix to treat wf edges
         // at the beginning
         wf.insert(wf.begin(),adc_samples->end()-low_board,adc_samples->end());
         // at the end
         wf.insert(wf.end(),adc_samples->end()-up_board,adc_samples->end());

         // prepare filter kernels
         size_t nsamples=wf.size();
         MA.resize(nsamples);
         AR.resize(nsamples);
         AR[0]=static_cast<double>(wf.back());
      
         size_t i, j, k;
         double tmp;
         for( i=0; i<nsamples; ++i)
            {
               if( (int(i - ceiled) >= 0) && (i + halfsize < nsamples) )  // take care of waveform edges
                  {
                     tmp = 0.; // zero partial summation 
                     for(j=0; j<kgsize; ++j) // cross-correlation filtering for the fast peak
                        {
                           tmp += static_cast<double>(wf.at( i + halfsize - j )) * gker[j];
                        }
                     MA[i] = tmp; 
                  }
               else
                  MA[i] = 0.;

               // Auto-Recursive filtering for the exponential tail
               if( i ) AR[i] = static_cast<double>(wf.at(nsamples-1-i)) + _a*AR[i - 1];
            }

         // ARMA: put together
         for(i = low_board; i<nsamples-up_board; ++i)
            {
               k = i-low_board;
               hit[k]=static_cast<uint16_t>( _scale_factor * AR[nsamples-1-i] * _tau  + scale_factor * MA[i] );
            }
         return &hit;
      }

   virtual inline void Reset(size_t nsamples)
   {
      if( nsamples <= kgsize )
         std::cerr<<"ARMA Reset: Number of samples: "<<nsamples
                  <<" smaller than the Gaussian kernel size: "<<kgsize<<std::endl;
      hit.resize(nsamples);
   }

   inline void SetScaleFactor(double f) { scale_factor = f; _scale_factor = 1. - scale_factor; }


 private:
   void gen_gaus_kernel()
   {
      gker.clear();
      kgsize = static_cast<int>(8*fsigma);
      for(size_t i=0; i<kgsize; ++i)
         gker.push_back(exp( -0.5*pow( (i-4.*fsigma)/fsigma,2) ) / (fsigma*sqrt(2*M_PI)) );
   }
  
   void FixParameters()
   {
      ceiled = ceil(kgsize / 2 ) + 1;
      halfsize = kgsize / 2;
    
      scale_factor = ffp_exp_ratio/ftau*(sqrt(2*M_PI)*fsigma);
      _scale_factor = 1. - scale_factor;
      _tau =  1./ftau;
      _a = 1. - _tau;
      fboard = static_cast<int>(ftau);
      low_board = flow*fboard;
      up_board = fup*fboard;
   }

  
 private:
   // filter parameters
   double ftau; // in samples
   double fsigma; // in samples
   double ffp_exp_ratio;

   // convenience variables
   double _a;
   double _tau; // 1/tau : 1/samples

   // wf boarding
   size_t fboard; // samples
   size_t flow;
   size_t fup;
   // number of samples for boarding 
   size_t low_board; // samples
   size_t  up_board; // samples

   // combine (1-scale_factor)*AR+scale_factor*MA
   double scale_factor;
   double _scale_factor;

   // gaussian kernel vector
   std::vector<double> gker;
   size_t kgsize;
   size_t ceiled;
   size_t halfsize;

   // finite response vector
   std::vector<double> MA;
   // Auto-Recursive vector
   std::vector<double> AR;
};


/* emacs
 * Local Variables:
 * tab-width: 8
 * c-basic-offset: 3
 * indent-tabs-mode: nil
 * End:
 */
